# Sistema de control Brazo robotico 
![Imagen no disponible](Imagenes%20/vista_inferior.jpg "Vista inferior")	

## Descripción
Este sistema permite controlar en tiempo real un brazo robótico al cual se le pueden programar una serie de instrucciones o movimientos. El brazo robótico usado se compone de 5
ejes de movimiento, cada uno de los ejes es controlado por un servo motor, a excepción de la base la cual es controlada por un motor a pasos. 
Las instrcciones a realizar se pueden indicar mediante una interfaz gráfica en Java, dicha interfaz permite mover cada eje, listar los movimientos a enviar y por último
enviar dichos movimientos al arnuido para que sean almacenados en la memoria EEPROM con el objetivo de poder ejecutar los movimientos despues de desconectar
y conectar la corriente electrica. Asi mismo, el brazo róbotico puede ser controlado mediante una aplicación en android, dicha aplicación se conforma de botones mover al robot tal como si de un control remoto se tratara.



## Datos de los Alumnos
**Instituto Tecnológico de León** 

**CARRERA:** Ingeniería en Sistemas Computacionales

**Practica 4: Brazo Robótico de 5 Ejes**

**Materia:** Sistemas Programables

**Profesor:** Ing. Levy Rojas Carlos Rafael

**Integrantes:** 

* José Guadalupe de Jesús Cervera Barbosa.

* Miguel Ángel Ramírez Lira. 

* Alfredo Validivia Barajas. 

***

# Prerrequisitos
## Programas necesarios📋


*    El Arduino IDE el cual se puede descargar de la pagina oficial: [Pagina de Arduino](https://www.arduino.cc/en/Main/Software)  
*    Git ya sea en linux o windows en caso de querer clonar el repositorio.
*    Netbeans con OpenJDK el cual puede descargarse desde la tienda de Ubuntu
*    Libreria PanamaHitek_Arduino-3.0.0.jar (Ya se encuentra dentro del proyecto de java en /src/librerias/)
*    Libreria RXTXcomm.jar (Ya se encuentra dentro del proyecto de java en /src/librerias/)
*    El IDE de [Android Studio](https://developer.android.com/studio/) para correr el código fuente incluido.
*    Se puede prescindir de Android Studio instalando el APK que también está en el repositorio

### Instalación de GIT
para instalar git solo se tiene que abrir una terminal e introducir el siguiente comando
```
$ apt-get install git
```


## Materiales: 🔧

*    2 Protoboard.
*    1 Arduino MEGA (o alguno equivalente).
*    1 Display LCD (En este ejemplo se usó uno de 20*4)
*    4 Servomotores
*    1 motor a pasos
*    1 Módulo de bluetooth HC05 o HC06
*    3 botones push
*    Cables 


***


## Como probar el programa
### 1. clonar el repositorio
Una vez que se tenga instalado Git en nuestra computadora, se debe:

* Crear una carpeta o un directorio donde se desee clonar el repositorio en nuestra computadora, Abrir la terminal desde la ruta actual e iniciar un nuevo repositorio con el siguiente comando
```
$ git init

```

* clonar el repositorio ejecutando el siguiente comando
```
$ git clone https://JCerver@bitbucket.org/El_Alfredo97/biceprobotico.git

```
Y es todo ya tendrás clonado el repositorio en tu directorio.

***

### 2. Abrir archivo .ino desde el IDE arduino 
Desde el IDE de arduino abrir el archivo ArduinoBicepRobotico.ino, es importante que el archivo este contenido en una carpeta con el mismo nombre ya que arduino no lo abrirá si no esta en esta forma, en este caso el archivo ya se encuentra dentro de la carpeta "ArduinoBicepRobotico"


### 3. Importar librerías necesarias en Arduino IDE
#### Es necesario importar las liberías que se muestran en la siguiente lista, todas están en el gestor de librerías del IDE para descargar:

* \#include <EEPROM.h>
* \#include "Movimiento.h"  (Esta se encuentra en el mismo repositorio hecha por nosotros)
* \#include "StringSplitter.h"
* \#include <LiquidCrystal.h>
* \#include<Servo.h>

### Para importar una libreria desde arduino necesitamos ubicarnos en la opción Programa y dentro de está dirigirnos a la opcion "Incluir libreria":
![Imagen no disponible](Imagenes%20/1.png)

#### Ya estando dentro, procedemos a incluir las librerias previamente mencionadas, a continuación se anexan unos ejemplos de como realizarlo
#### Libreria LiquidCrystal
![Imagen no disponible](Imagenes%20/2.png)

#### Libreria Servo
![Imagen no disponible](Imagenes%20/3.png)

#### Si necesitamos descargar alguna libreria que no este incluida en el IDE de Arduino, nos dirigimos a la opción de "Gestionar librerías"
![Imagen no disponible](Imagenes%20/4.png)

#### Debe aperecer un panel similar al de la imagen, a continuación procedemos a escribir el nombre de la libreria necesaria y presionamos instalar, para este ejemplo se decargó la libreria StringSplitter
![Imagen no disponible](Imagenes%20/5.png)

####   Además es necesario cambiar un valor de un archivo de la librería para que se pueda hacer split de las cadenas de texto en más de 5.
####   El máximo por default es 5, lo cambiamos a 80 como se muestra en la imagen
![Imagen no disponible](Imagenes%20/splitterFile.png )

### 4. Cargar el programa a tu arduino

### 5. Abrir el proyecto de Java dede Netbeans

#### Importar las siguientes librerias (Lo cuál no es necesario porque ya se encuentran dentro del proyecto de JAVA):
* PanamaHitek_Arduino-3.0.0.jar
* RXTXcomm.jar

#### Ubicar el puerto donde este conectado Arduino
Dentro del proyecto de Java, dirigete a la clase "ControladorArduino.java" ubicada en el mismo proyecto en la ruta "/src/Controlador", en esta clase se debe especificar el nombre del puerto mediante el cual se está conectando el arduino que tenemos conectado:

#### Ejecutar el programa 
Para esto es indispensable que el Arduino este conectado a tu computadora y con el programa de Arduino ya cargado previamente en él. Y listo ya podrás interactuar con la interfaz gráfica del programa o el teclado matricial.

### 6. Abrir aplicación móvil
Instalar el APK que se encuentra en la carpeta de "Apk" en tu dispositivo Android. O bien puedes correr el código fuente en Android Studio desde la carpeta "BluetoothArduino" y carga la aplicación atu móvil. Quizá sea necesario modificar la interfaz para algunos modelos de teléfonos ya que podría diferir la visualización de los componentes en diferentes pantallas.

#### Conectar el móvil al módulo HC05
Al abrir la aplicación de Android aparecerán los dispositivos vinculados, por loq ue será necesario primero vincular el HC05 desde la configuración Bluetooth de tu equipo para que aparezca en la aplicación. Luego seleccionas el nombre de HC05, esperas algunos segundos y podrás ver la interfaz donde podrás enviar mensajes a la pantalla LCD, calibrar el brillo y contraste de la misma y además podrás ver la temperatura, humedad y cantidad de luz en la pantalla del equipo.

## Diagrama del circuito.
Diagrama de circuito: 
![Imagen no disponible](Diagrama/diagramaBrazo.png "Diagrama de circuito")	

## Vista del circuito armado:
Este es el resultado al armar el circuito mostrado en el diagrama de arriba

####Vista general:
![Imagen no disponible](Imagenes%20/vista_general.jpg)	

####Vista lateral:
![Imagen no disponible](Imagenes%20/vista_lateral.jpg)	

####Vista inferior:
![Imagen no disponible](Imagenes%20/vista_inferior.jpg)	

####Mensaje de bienvenida del LCD:
![Imagen no disponible](Imagenes%20/lcd_bienvenida.jpg)	

####Mensaje de paro de emergencia del LCD:
![Imagen no disponible](Imagenes%20/lcd_emergencia.jpg)	

####Mensaje posición incial del LCD:
![Imagen no disponible](Imagenes%20/lcd_posicioninicial.jpg)	


## Vista del circuito con funcionalidad extra (con funcionalidad Bluetooth):
 
# Conceptos técnicos
Si quieres saber como funciona el programa es necesario conocer algo de teoria para conocer que existe detrás de la magia:

## Memoria EEPROM
### ¿Qué es?
Un Arduino dispone de una memoria EEPROM, es decir, una memoria no volátil (como la de un disco duro) en la que se pueden almacenar datos sin preocuparte de perderlos cuando se le quites la alimentación al Arduino.

Utilizar la EEPROM de tu placa es especialmente útil a la hora de guardar la configuración de tu Arduino como por ejemplo las direcciones MAC e IP


### Capacidad De La Memoria EEPROM del Arduino
En nuestro caso se tiene un arduino MEGA por lo que la capacidad de almacenamiento en la EEPROM es de 4KB tal como se observa en la imagen:

![alt text](Imagenes%20/tablaCapacidadEeprom.jpg "Motor a pasos 28BYJ-48 con driver ULN2003")



### Guardar y leer datos de Memoria EEPROM
Al inicio de este documento se menciono que se debia de importar la libreria EEPROM.h, esto en el archivo de arduino:

```
#include <EEPROM.h>

```
Cada posicon o dirección de la memoria EEPROM permite alamcenar un byte por lo que el número decimal mas grande que se puede alamcenar son 255
Esta libreria nos permitirá escribir datos en la memoria EERPOM usando el siguiente método (cabe mencionar que es nuestro caso almacenamos byte por byte, es decir en cada posicion o direccion almacenamos un byte):

```
EEPROM.write(direccion,informacion);

```

Para obtener el valor almacenado en esa dirección se usa (esto nos devolveria el byte que se almanceno en dicha posición):

```
EEPROM.read(direccion);

```


http://www.educachip.com/como-usar-la-memoria-eeprom-de-arduino/
Para concer mas sobre almacenar datos en la memoria EEPROM te recomendamos consultar la fuente:
[Cómo usar la memoria EEPROM de arduino](http://www.educachip.com/como-usar-la-memoria-eeprom-de-arduino/)


***






## Motor a Pasos
### ¿Qué es?
El 28BYJ-48 es un pequeño motor paso a paso unipolar.

 El 28BYJ-48 tiene un paso de 5.625 grados (64 pasos por vuelta). El reductor interno tiene una relación de 1/64. Combinados, la precisión total es de 4096 pasos por vuelta, equivalente a un paso de 0.088º, que es una precisión muy elevada.

En realidad la relación del reductor no es exactamente 1/64 por lo que el número de pasos es 4076 por vuelta (equivalente a un reductor de 1/63.6875)


![Motor a pasos](Imagenes%20/motorAPasos.png "Motor a pasos 28BYJ-48 con driver ULN2003")

### ¿Cómo funciona?


 Aplicando un control todo o nada, es decir, encendiendo por completo o apagando por completo una bobina, únicamente tenemos que activar las bobinas en la secuencia correcta.

Existen varias secuencias posibles, en nuestro caso la secuencia que funciono fue la siguiente:

** cuencia medio pasos **
La secuencia en medio paso (half-step). Aquí se enciende alternativamente uno y dos bobinas.

Con esta secuencia conseguimos una precisión de la mitad del paso. El par desarrollado varía ya que en algunos pasos activamos dos bobinas y en otras solo una, pero a la vez el giro se encuentra más “guiado”, por lo que en general ambos efectos se compensan y el funcionamiento es bueno, salvo en aplicaciones donde estemos muy al límite del par máximo.

![Secuencia de movimientos](Imagenes%20/secuenciaMedioPaso.png "Secuencia media fase")



![Secuencia de movimientos](Imagenes%20/tablaSecuencia.png "Tabla de secuencia de media fase de un motor a pasos")



Para mas información consultar la fuente:
[Motor 28BYJ-48 y un UNL2003](https://www.luisllamas.es/motor-paso-paso-28byj-48-arduino-driver-uln2003/)



***





## Servo Motor
### ¿Qué es?
Un servomotor es un tipo especial de motor que permite controlar la posición del eje en un momento dado. Esta diseñado para moverse determinada cantidad de grados y luego mantenerse fijo en una posición.

Para comprender esto haremos una comparación con un motor DC
Los motores DC como los que se muestran en la imagen tienen la particularidad de que giran sin detenerse. No son capaces de dar determinada cantidad de vueltas o detenerse en una posición fija. Solo giran y giran sin parar.


Por el cotrario los servo motores se caracterizan por ser permitir un movimiento controlado y por entregar un mayor par de torsión (torque) que un motor DC común. 

un servomotor se hace referencia a un sistema compuesto por componentes electromecánicos y electrónicos. El circuito electrónico es el encargado de manejar el movimiento y la posición del motor.

![alt text](Imagenes%20/interiorServoMotor.jpg "Mencanismo de engranes en el interior de un servo motor")



Para mas información consultar la fuente:
[¿Qué es y cómo funciona un servomotor](http://panamahitek.com/que-es-y-como-funciona-un-servomotor/)



***


## Display LCD

### ¿Qué es?
LCD es el acrónimo de Liquid Crystal Display (en español Pantalla de Cristal Líquido). 

### ¿Cómo funciona?
Un LCD utiliza las propiedades de la luz polarizada para mostrarnos la información en una pantalla. A partir de una serie de filtros, se consigue mostrar la información gracias a la iluminación de fondo.

Hay una amplia gama de pantallas LCDs que podemos utilizar con Arduino. Aparte de las funcionalidades extra que nos puedan dar cada una de ellas, las podemos diferenciar por el número de filas y columnas, su tamaño.

Para mas información consultar la fuente:
[Texto en movimiento en un LCD con Arduino](https://programarfacil.com/tutoriales/fragmentos/arduino/texto-en-movimiento-en-un-lcd-con-arduino/)

***

# Autores ✒️
* **José Guadalupe de Jesús Cervera Barbosa** - *Trabajo general* - 
* **Miguel Ángel Ramírez Lira** - *Trabajo general* - 
* **Alfredo Valivia Barajas** -*Trabajo general* - 






