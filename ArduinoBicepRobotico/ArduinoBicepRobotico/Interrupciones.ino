

void interrupcionRepetirMovimiento() {
  /*Este método es usado cuando se activa la interrupcion al presionar el BOTON_REPETIR_MOVIMIENTO para repetir pasos o movimientos almacenados
    (El botón simula un boton de permite ejecutar nuevamente los movimientos almacenados)*/

                                                          /*Condicional para eliminar el rebote de la interrupción:   */
  if (millis() > contadorTiempo + TIEMPO_DE_UMBRAL) {
    contadorTiempo = millis();
    ejecutando = 1; //se hace un cambio del valor de variable ejecutando (pone valor en 1)
    showDisplay(3, 0, 0, 0);                                //se envia al display el mensaje de la acción realizada


  }
}


void interrupcionParoEmergencia() {
  /*Este método es usado cuando se activa la interrupcion al presionar el BOTON_PARO_EMERGENCIA para forzar que se detenga el movimiento del brazo robotio
    (El botón simula un boton de paro de emergencia que detiene la ejecucion de los movimientos almacenados)*/

  /*Condicional para eliminar el rebote de la interrupción:   */
  if (millis() > contadorTiempo2 + TIEMPO_DE_UMBRAL2) {
    contadorTiempo2 = millis();
    ejecutando = 0; //se hace un cambio del valor de variable ejecutando (pone valor en 0)
    showDisplay(2, 0, 0, 0);                                //se envia al display el mensaje de la acción realizada
  }
}

void interrupcionPosicionar() {
  /*
Este método es usado cuando se activa la interrupcion al presionar el BOTON_POSICION_INCIAL colocar los servos en las psiciones por defecto inidicadas por el metodo "colocarEnPosicionInicial()"  
*/

  /*Condicional para eliminar el rebote de la interrupción:   */
  if (millis() > contadorTiempo3 + TIEMPO_DE_UMBRAL3) {
    contadorTiempo3 = millis();
    showDisplay(5, 0, 0, 0);                                //se envia al display el mensaje de la acción realizada

    colocarEnPosicionInicial();                             //Se coloca a los servos en las posiciones iniciales o por default
  }
}

