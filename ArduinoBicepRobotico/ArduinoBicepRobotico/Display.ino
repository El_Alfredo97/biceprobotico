

void setupDisplay() {
  lcd.begin(20, 4);                                                         //método para iniciar el display (Recibe: anchura y altura de caracteres))
  Serial.setTimeout(50);
  showDisplay(1, 0, 0, 0);                                                  //coloca el mensaje inicial en el display                  
  analogWrite(PIN_BRILLO, 128);
  analogWrite(PIN_CONTRASTE, 124);


}



void showDisplay(int opcion, int paso, int eje, int grado) {
/*
Este metodo permite conocer que mnsaje se debe de mostrar en el display lcd
de acuerdo a la variable "opcion" se pueden tener los siguientes mensajes:

  //bienvenida                        1
  //paro emergencia                   2
  //repetir movientos alamcenados     3
  //ejecutando movimietnos            4
  //regresar ejes a posicion incial   5

*/
  



  lcd.clear();

  switch (opcion) {                                                           //se verifica que tipo de mensaje se desea mostrar en el display
    case 1:
      showMENSAJE_BIENVENIDA();
      break;

    case 2:
      showMENSAJE_PARO_EMERGENCIA();
      break;

    case 3:
      showMensajeRepetir();
      break;

    case 4:
      showMENSAJE_EJEcutando(paso, eje, grado);
      break;

    case 5:
      showMENSAJE_POSICION_INICIAL();
      break;
  }


}


void showMENSAJE_BIENVENIDA() {

  /*
  Este tipo de mensaje se muestra al inicio del setup principal, solo da una bienvenida
  */
  lcd.setCursor(0, 0);
  String linea1 = MENSAJE_BIENVENIDA.substring(0, 20);                        //si es muy grande se particiona en dos filas
  String linea2 = MENSAJE_BIENVENIDA.substring(21, 40);

  lcd.setCursor(0, 0);
  lcd.print(linea1);
  lcd.setCursor(0, 1);
  lcd.print(linea2);

}

void showMENSAJE_PARO_EMERGENCIA() {
  /*
  Este tipo de mensaje se cuando se presiona el boton de paro de emergecia
  */
  lcd.setCursor(0, 0);
  String linea1 = MENSAJE_PARO_EMERGENCIA.substring(0, 20);
  String linea2 = MENSAJE_PARO_EMERGENCIA.substring(21, 40);

  lcd.setCursor(0, 0);
  lcd.print(linea1);
  lcd.setCursor(0, 1);
  lcd.print(linea2);
}

void showMensajeRepetir() {
  /*
  Este tipo de mensaje se muestra cuando se presiono el boton para ejecutar movimientos previamente almacenados
  */
  
  lcd.setCursor(0, 0);
  String linea1 = MENSAJE_REPETIR_MOVIMIENTO.substring(0, 20);
  String linea2 = MENSAJE_REPETIR_MOVIMIENTO.substring(21, 40);

  lcd.setCursor(0, 0);
  lcd.print(linea1);
  lcd.setCursor(0, 1);
  lcd.print(linea2);

}

void showMENSAJE_EJEcutando(int paso, int eje, int grado) {
  /*
  Este tipo de mensaje se muestra MIENTRAS se esta ejecutando los movimientos recibidos desde java o almacenados en el arduino
  Se recibe el numero del movimiento que se esta ejecutando, el eje, y el grado
  */
  
  String ejeMov = "";
  lcd.setCursor(0, 0);
  lcd.print(MENSAJE_EJECUCION);
                                                                              //Se verifica que eje es el que se esta moviendo y se guarda en un String
  switch (eje) {
    case 1:
      ejeMov = "Pinza";
      break;
    case 2:
      ejeMov = "Giro Muneca";
      break;
    case 3:
      ejeMov = "Vert Muneca";
      break;
    case 4:
      ejeMov = "Codo";
      break;
    case 5:
      ejeMov = "Hombro";
      break;

  }

                                                                              //Se imprime en la segunda linea el numero del paso que se esta ejecutando
  MENSAJE_MOVIMIENTO = "";
  MENSAJE_MOVIMIENTO += MENSAJE_MOVIMIENTO_COMODIN;
  MENSAJE_MOVIMIENTO += " ";
  MENSAJE_MOVIMIENTO += paso;
  lcd.setCursor(0, 1);
  lcd.print(MENSAJE_MOVIMIENTO);

                                                                              //Se imprime en la tercer linea el eje que se esta moviendo
  MENSAJE_EJE = "";
  MENSAJE_EJE += MENSAJE_EJE_COMODIN;
  MENSAJE_EJE += " ";
  MENSAJE_EJE += ejeMov;
  lcd.setCursor(0, 2);
  lcd.print(MENSAJE_EJE);

                                                                              //Se imprime en la cuarta linea el grado al que se esta moviendo el eje
  MENSAJE_GRADO = "";
  MENSAJE_GRADO += MENSAJE_GRADO_COMODIN;
  MENSAJE_GRADO += " ";
  MENSAJE_GRADO += grado;
  lcd.setCursor(0, 3 );
  lcd.print(MENSAJE_GRADO);
}


void showMENSAJE_POSICION_INICIAL() {
/*
  Este tipo de mensaje se muestra cuando se presiono el boton para reubicar los ejes en sus posiciones por defecto
  */
  
  lcd.setCursor(0, 0);
  String linea1 = MENSAJE_POSICION_INICIAL.substring(0, 20);
  String linea2 = MENSAJE_POSICION_INICIAL.substring(21, 40);

  lcd.setCursor(0, 0);
  lcd.print(linea1);
  lcd.setCursor(0, 1);
  lcd.print(linea2);


}


