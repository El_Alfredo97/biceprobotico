

void setupMotorAPasos()
{
  //declarar pines como salida
  pinMode(MOTOR_PIN_1, OUTPUT);
  pinMode(MOTOR_PIN_2, OUTPUT);
  pinMode(MOTOR_PIN_3, OUTPUT);
  pinMode(MOTOR_PIN_4, OUTPUT);
}

void loopMotorAPasos() {
  for (int i = 0; i < stepsPerRev ; i++) {
    clockwise();
    delayMicroseconds(2000);
  }

  for (int i = 0; i < stepsPerRev; i++) {
    anticlockwise();
    delayMicroseconds(motorSpeed);
  }
  delay(1000);
}

void clockwise()
{
  /*
  Este metodo mueve en sentido horario al motor a pasos
  
  */
  stepCounter++;
  if (stepCounter >= NUM_STEPS) stepCounter = 0;
  setOutput(stepCounter);
}

void anticlockwise()
{
  /*
  Este metodo mueve en sentido antihorario al motor a pasos
  
  */
  stepCounter--;
  if (stepCounter < 0) stepCounter = NUM_STEPS - 1;
  setOutput(stepCounter);
}

void stepMotorMove(int pos) {
/*
Este método se encarga de recibir la posicion hacia la que se desea mover,
Si el grado al que se quiere mover es mayor al de la posicion actual. se gira en sentido horario hacia el grado deseado
Si el grado al que se quiere mover es menor al de la posicion actual. se gira en sentido antihorario hacia el grado deseado
*/
  if (positionCont > pos) {
    for (positionCont; positionCont > pos ; positionCont--) {
      anticlockwise();
      delayMicroseconds(motorSpeed);
    }
  } if (positionCont < pos) {
    for (positionCont; positionCont < pos; positionCont++) {
      clockwise();
      delayMicroseconds(motorSpeed);
    }
  }
}

void setOutput(int step)
{
  digitalWrite(MOTOR_PIN_1, bitRead(STEPS_LOOKUP[step], 0));
  digitalWrite(MOTOR_PIN_2, bitRead(STEPS_LOOKUP[step], 1));
  digitalWrite(MOTOR_PIN_3, bitRead(STEPS_LOOKUP[step], 2));
  digitalWrite(MOTOR_PIN_4, bitRead(STEPS_LOOKUP[step], 3));
}
