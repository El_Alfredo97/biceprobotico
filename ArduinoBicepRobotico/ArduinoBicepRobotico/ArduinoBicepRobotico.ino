/************************************************
  Programa: Sistema autónomo, brazo robot programable

  Autores:
  José Guadalupe de Jesús Cerera Barbosa.
  Miguel Ángel Ramírez Lira.
  Alfredo Valdivia Barajas.

  El programa permite cargar en arduino el sistema que permitirá que un brazo robótico
  con 5 ejes o grados de libertad pueda ser controlado y programado con una serie de
  pasos o movimientos a seguir desde una aplicación desarrollada en Java
  y otra desarrollada en Android.
 ***********************************************/


/****************      IMPORTAR LIBRERIAS NECESARIAS      **************** */
#include <EEPROM.h>                                         //añadir al libreria para poder almacenar bytes en la memoria EEPROM
#include "Movimiento.h"                                     //Importar el objeto Movimiento.h creado por nosotros para alamcenar parámetros de movimientos
#include "StringSplitter.h"                                 //añadir al libreria para permitir la división de una cadena separada por comas
#include <LiquidCrystal.h>                                  //añadir al libreria para poder mandar mensajes a un DISPLAY LCD
#include<Servo.h>                                           //añadir al libreria para poder controlar el movimiento de los ServoMotores

/******************************************************************************************************************
          VARIABLES PARA LA MEMORIA EEPROM
******************************************************************************************************************/

/************************** VARIABLES  ****************************/
String cadenaMovimientos = "";
String cadenaEprom = "";                                      //Variable para leer y escribir bytes de movimiento en memoria EEPROM
int ejecutando = 0;



/******************************************************************************************************************
          VARIABLES Y CONSTANTES PARA LAS INTERRUPCIONES DE BOTONES
******************************************************************************************************************/

/****************         BOTONES DE INTERRUPCIONES       *******************/

#define BOTON_PARO_EMERGENCIA 21                            //Botón que permite el paro de emergencia
#define BOTON_REPETIR_MOVIMIENTO 20                         //Botón que permite repetir movimientos almacenados
#define BOTON_POSICIONAR 19                                 //Botón que permite repetir movimientos almacenados

const int TIEMPO_DE_UMBRAL = 100;                           //Constante que se usará para eliminar el rebote de la interrupcion de BOTON_PARO_EMERGENCIA
long contadorTiempo = 0;                                    //Variable usada en la interrupcion para almacenar el valor de millis()

const int TIEMPO_DE_UMBRAL2 = 100;                          //Constante que se usará para eliminar el rebote de la interrupcion de BOTON_REPETIR_MOVIMIENTO
long contadorTiempo2 = 0;                                   //Variable usada en la interrupcion para almacenar el valor de millis()

const int TIEMPO_DE_UMBRAL3 = 100;                          //Constante que se usará para eliminar el rebote de la interrupcion de BOTON_POSICIONAR
long contadorTiempo3 = 0;                                   //Variable usada en la interrupcion para almacenar el valor de millis()



/******************************************************************************************************************
          CONSTANTES PARA LCD DISPLAY
******************************************************************************************************************/

/****************               CONSTANTES                    *******************/
//para arduino MEGA
#define RS 2                                                //pin de conexión a punto RS del LCD display
#define E  3                                                //pin de conexión a punto E del LCD display
#define D4 4                                                //pin de conexión a punto D4 del LCD display
#define D5 5                                                //pin de conexión a punto D5 del LCD display
#define D6 6                                                //pin de conexión a punto R6 del LCD display
#define D7 7                                                //pin de conexión a punto R7 del LCD display
#define PIN_BRILLO A0                                       //pin analógico para brillo de LCD
#define PIN_CONTRASTE A1                                    //pin analógico para contarste de LCD

/****************         Constructor para gestionar el LCD Diaplay       *******************/
LiquidCrystal lcd(RS, E, D4, D5, D6, D7);


                                                            //MENSAJES QUE SE MOSTRARÁN EN EL DISPLAY LCD
                                                            //mensaje de bienvenida
const String MENSAJE_BIENVENIDA = "Sistema autonomo robotico";

                                                            //mensajes que se mostrarán mientras el brazo ejecute los movimientos almancenados
const String MENSAJE_EJECUCION = "Ejecutando";
const String MENSAJE_MOVIMIENTO_COMODIN = "Paso numero: ";
const String MENSAJE_MOVIMIENTO = "";
const String MENSAJE_EJE_COMODIN = "Eje: ";
const String MENSAJE_EJE = "";
const String MENSAJE_GRADO_COMODIN = "Grado: ";
const String MENSAJE_GRADO = "";

                                                            //mensaje que se mostrará cuando se presione el boton BOTON_PARO_EMERGENCIA
const String MENSAJE_PARO_EMERGENCIA = "Paro de emergencia";

                                                            //mensaje que se mostrará cuando se presione el boton BOTON_REPETIR_MOVIMIENTO
const String MENSAJE_REPETIR_MOVIMIENTO = "Preparando Movimientos";

                                                            //mensaje que se mostrará cuando se presione el boton BOTON_POSICIONAR
const String MENSAJE_POSICION_INICIAL = "Posicion Inicial";



/******************************************************************************************************************
          VARIABLES Y CONSTANTES PARA MOTOR A PASOS
******************************************************************************************************************/

/****************         CONSTANTES       *******************/
//definicion de pines
const int MOTOR_PIN_1 = 36;                                   // 28BYJ48 In1
const int MOTOR_PIN_2 = 37;                                   // 28BYJ48 In2
const int MOTOR_PIN_3 = 38;                                   // 28BYJ48 In3
const int MOTOR_PIN_4 = 39;                                   // 28BYJ48 In4

//secuencia media fase
const int NUM_STEPS = 8;
const int STEPS_LOOKUP[8] = { B1000, B1100, B0100, B0110, B0010, B0011, B0001, B1001 };

/****************         VARIABLES       *******************/
int motorSpeed = 800;                                         //variable para fijar la velocidad
int stepCounter = 0;                                          // contador para los pasos
int stepsPerRev = 4076;                                       // pasos para una vuelta completa
int positionCont = 2000;                                      //posición del motoren vueltas



/******************************************************************************************************************
          VARIABLES Y CONSTANTES PARA SERVO MOTORES
******************************************************************************************************************/
Servo pinza;
Servo giroMuneca;
Servo verticalMuneca;
Servo codo;

/****************         CONSTANTES       *******************/
//definicion de pins
const int SERVO_PIN_PINZA = 33;                               //Pin conectado al servo pinza
const int SERVO_PIN_GIRO_MUNECA = 32;                         //Pin conectado al servo giroMuneca
const int SERVO_PIN_VERTICAL_MUNECA = 31;                     //Pin conectado al servo verticalMuneca
const int SERVO_PIN_CODO = 30;                                //Pin conectado al servo codo

//Valores maximos y minimos de movimiento de cada servo
const int MAX_PINZA = 137;                                    //Grado máximo de movimiento para el servo pinza
const int MIN_PINZA = 36;                                     //Grado minimo de movimiento para el servo pinza
const int MAX_GIRO_MUNECA = 180;                              //Grado máximo de movimiento para el servo giroMuneca
const int MIN_GIRO_MUNECA = 0;                                //Grado minimo de movimiento para el servo giroMuneca
const int MAX_VERTICAL_MUNECA = 180;                          //Grado máximo de movimiento para el servo verticalMuneca
const int MIN_VERTICAL_MUNECA = 30;                           //Grado minimo de movimiento para el servo verticalMuneca
const int MAX_CODO = 180;                                     //Grado máximo de movimiento para el servo codo
const int MIN_CODO = 0;                                       //Grado minimo de movimiento para el servo codo

/****************         VARIABLES       *******************/
//arreglo para almancenar los movimientos a realizar (los datos de los objetos de este arreglo son los que se guardan en la EEPROM)
Movimiento movimientos[200];                                  //Arreglo que almacenará objetos de tipo Movimiento
int sizeMovimientosReal = 1;                                  //Cantidad total(real) de movimientos almacenados en el arreglo movimientos[200]


byte BufferIn[4];
boolean StringCompleta = false;                               //variable booleana para saber cuando se recibio el string de movimientos completo
boolean ValPWMLeido = false;

byte Cont_char = 0;                                           //variable usada para saber si se recibieron los datos necesarios desde el Serial

//varialbles usadas para alamacenar la posicion en la que se quedo cada servo motor 
int posicionPinza = 0;
int posicionGiroMuneca = 90;
int posicionVerticalMuneca = 90;
int posicionCodo = 90;


void setup() {
  Serial.begin(9600);                                         //iniciamos la comunicacion Serial
  
  //modos de los pines necesarios
  pinMode(BOTON_PARO_EMERGENCIA, INPUT_PULLUP);
  pinMode(BOTON_REPETIR_MOVIMIENTO, INPUT_PULLUP);
  pinMode(BOTON_POSICIONAR, INPUT_PULLUP);
  
  //  Las interrupciones a utilizar y los metodos para manejar dichas interrupciones:
  attachInterrupt(digitalPinToInterrupt(BOTON_REPETIR_MOVIMIENTO), interrupcionRepetirMovimiento, RISING);    //rising es el modo de disparo que activara la interrupcion
  attachInterrupt(digitalPinToInterrupt(BOTON_PARO_EMERGENCIA), interrupcionParoEmergencia , RISING);         //rising es el modo de disparo que activara la interrupcion
  attachInterrupt(digitalPinToInterrupt(BOTON_POSICIONAR), interrupcionPosicionar , RISING);                  //rising es el modo de disparo que activara la interrupcion


  leerMovEEPROM();                                            //leer los movimientos almacenados en la EEPROM, este método los almacena en variable cadenaEprom
  setupDisplay();                                             //Iniciar parametros iniciales como el mensaje incial, brillo y contraste
  setupMotorAPasos();                                         //Iniciar valores inciales para el motor a pasos, como los el modo de los pines del motor                                         
  setupCervos();                                              //Iniciar valores iniciales (asociacion de pines con cada servo y la posicion inicial de los servos)

}

void loop() {


  
  if (ejecutando == 1) {                                      //se verifica si el brazo esta o estaba realizando alguna secuencia de movimientos y reaunuda la ejecucion de movimientos
    separarCadena(cadenaEprom);                               //se separa la cadenaEprom la cual contiene los parametros de cada movimiento separado por comas

    delay(3000);                                              //tiempo de espera para mostrar el mensaje MENSAJE_REPETIR_MOVIMIENTO
    loopServos();                                             //inicia los movimientos de cada servo de acuerdo a la secuencia de movimientos 
    ejecutando = 0;                                           //se indica que se termino de ejecutar toda la lista de movimientos
  }

                                                              //ESTE IF SOLO FUNCIONA CUANDO SE HA RECIBIDO DATOS DEL EXTERIOR, YA SEA POR SERIAL O BLUETOOTH
                                                              //(BufferIn[0] sirve para comparar que datos se estan recibiendo   y    BufferIn[1] contiene los datos )
  if ((StringCompleta == true) && (Cont_char == 2))
  {                                                           //se verifica que se hayan recibido los datos completos 

    ValPWMLeido = true;
                                                              //------1         SI SE RECIBIO DATOS PARA MOVERSE EN TIEMPO REAL
    if (BufferIn[0] == 190) {                                 //si se recibio un 190 seguido del grado para el hombro lo mueve en tiempo real
      stepMotorMove(4076 / 254 * BufferIn[1]);                //mueve hombro a la posicion que se recibio en tiempo real
    } else if (BufferIn[0] == 191) {                          //si se recibio un 191 seguido del grado para el codo lo mueve en tiempo real
      codo.write(BufferIn[1]);                                //mueve codo a la posicion que se recibio en tiempo real

    } else if (BufferIn[0] == 192) {                          //si se recibio un 192 seguido del grado para el verticalMuneca lo mueve en tiempo real
      verticalMuneca.write(BufferIn[1]);                      //mueve verticalMuneca a la posicion que se recibio en tiempo real

    } else if (BufferIn[0] == 193) {                          //si se recibio un 193 seguido del grado para el giroMuneca lo mueve en tiempo real
      giroMuneca.write(BufferIn[1]);                          //mueve giroMuneca a la posicion que se recibio en tiempo real

    } else if (BufferIn[0] == 194) {                          //si se recibio un 194 seguido del grado para la pinza lo mueve en tiempo real
      pinza.write(BufferIn[1]);                               //mueve la pinza a la posicion que se recibio en tiempo real
    } else if (BufferIn[0] == 195) {                          //------2          SI SE RECIBIO UNA SECUENCIA DE MOVIMIENTOS DESDE JAVA

      cadenaEprom = Serial.readString();                      //guarda en la variable la secuencia de movimientos separados por comas
      guardarMovEEPROM(cadenaEprom);                          //guarda movimientos en memoria EEPROM antes que nada en caso de apagones
      ejecutando = 1;                                         //indica que se comenzará a ejecutar movimientos recibidos
      
    } else if (BufferIn[0] == 201) {                          //-------3           A PARTIR DE AQUI SE VALIDA CADA UNO DE LOS MOVIMIENTOS RECIBIDOS DESDE APK ANDROID POR BLUETOOTH
      moverHombroRel(0);                                      //                   DEPENDIENDO DEL BOTÓN QUE SE PRESIONA SE MUEVE UN SERVO EN ESPECÍFICO O EL MOTOR A PASOS
    } else if (BufferIn[0] == 202) {
      moverGiroMunecaRel(0);
    } else if (BufferIn[0] == 203) {
      moverCodoRel(0);
    } else if (BufferIn[0] == 204) {
      moverMunecaVerticalRel(0);
    } else if (BufferIn[0] == 205) {
      moverHombroRel(1);
    } else if (BufferIn[0] == 206) {
      moverGiroMunecaRel(1);
    } else if (BufferIn[0] == 207) {
      moverCodoRel(1);
    } else if (BufferIn[0] == 208) {
      moverMunecaVerticalRel(1);
    } else if (BufferIn[0] == 209) {
      moverPinzaRel(0);
    } else if (BufferIn[0] == 210) {
      moverPinzaRel(1);
    }

    StringCompleta = false;                                   //se indica que la cadena de movimientos ser recibio completamente
    Cont_char = 0;                                            //el contador de caracteres restantes se devuelve a cero
    ValPWMLeido = false;

    BufferIn[0] = 0;                                          //la opcion para conocer que dato se esta recibiendo se regresa a cero
    BufferIn[1] = 200;                                        //condicion para indicar que ya no hay mas caracteres por leer
  }
}


void serialEvent() {
/*
Este metodo se dispara cada vez que se recibe algun dato de la comuicacion Serial, 
hace la validacion que se recibio el primer Byte que indica el tipo de datos a recibir,
del segundo en adelante se indican los movimientos o algun otro dato recibido
*/

  if (ValPWMLeido == false)
  {

    while (Serial.available())                                //mientras se este recibiendo algo de la comunicacion serial
    {
      BufferIn[Cont_char] = (byte)Serial.read();              //se almacena en un contador cuandos bytes se recibieron, el primero indica el tipo de dato y del segundo en adelante contiene los movimientos
      if (BufferIn[1] != 200) {                               //cuando ya no haya nada mas por leer se indica que se recibio la cadena completa           
        StringCompleta = true;
      }
      Cont_char++;
    }
  }

}

void separarCadena(String cadena) {
/*
Método que sirve para dividir una cadena que se encuentra separada por comas, la cadena contiene datos de movimietos, cada 4 valores se forma un nuevo movimiento
un objeto Movimiento se forma por 4 parametros (eje, grado,velocidad,espera),
el "eje" indica que servomotor se debe mover, el "grado" indica la posicion a la que se debe mover, 
la "velocidad" es que tan rapido o lento hará los movimientos, y el parametro "espera" indica el tiempo de espera para pasar al sigueinte movimiento
cada que se lee un movimiento completo incrementa la variable sizeMovimientosReal (indica cantidad de movimientos leidos)
*/

  StringSplitter *splitter = new StringSplitter(cadena, ',', 80);   //Se lee toda la cadena y se particiona donde se encuentra comas ","
  int valores[splitter->getItemCount()];

  sizeMovimientosReal = 0;                                          //Se inicia en cero porque se han leido cero movimientos por el momento
  int nElementos = splitter->getItemCount();                        //guarda el total de elementos separados por splitter

  for (int i = 0; i < nElementos; i = i + 4) {                      //toma de 4 en 4 valores para crear objetos Movimiento y almacenarlo en el arreglo de movimientos                      
    Movimiento m(splitter->getItemAtIndex(i).toInt(), splitter->getItemAtIndex(i + 1).toInt(), splitter->getItemAtIndex(i + 2).toInt(), splitter->getItemAtIndex(i + 3).toInt());
    movimientos[sizeMovimientosReal] = m;
    sizeMovimientosReal++;

  }
}

void guardarMovEEPROM(String cadena) {
  /*
Este método recibe una cadena con una secuencia de movimientos separados por comas,
el proposito de este metodo es particionar la cadena guardando cada elemento en la memoria EEPROM, 
esto con la finalidad de tener alamcenada la información de los movimientos aunque se apague o desconecte la corriente de alimentacion  
  */
  
  StringSplitter *splitter = new StringSplitter(cadena, ',', 80);     //se separa la cadena cada que encuentre una coma

  int sizeEEPROM = splitter->getItemCount();
  EEPROM.write(0, sizeEEPROM);                                       //siempre se almacena en la EEPROM el total de elementos guardados, esto sirve para conocer el total de movimientos guardados

  for (int i = 2; i <= sizeEEPROM; i++) {                             //se comienza a alacenar byte por byte a partir de la posicion 2 (ya que en la 0(cero) se tiene el 
                                                                      //tamaño total de elementos y en la posciion 1(uno) se tiene el valor para saber si cuando 
                                                                      //se apago el arduino se estaba ejecutando un movimiento o no)
    int valor = splitter->getItemAtIndex(i - 2).toInt();
    EEPROM.write(i, valor);                                           //Se almacena cada byte en su posicion correspondiente
  }
}

void leerMovEEPROM() {
/*
Este método lee los bytes almacenados en la memoria EEPROM, 
el primer byte indica el total de elementos usados para los movimientos
el segundo byte indica un valor de 0(cero) cuando no se estaba ejecutando movimientos 
  o un 1(uno) para indicar que se estaba ejecutnado una secuencia de movimientos cuando se apago, desconecto o reinicio el arduino
del byte 2 en adelante se tienen los parametros usados para los movimientos (los datos de cada 4 bytes se usan para crear un Movimiento)
 */
  
  int sizeEEPROM;
  sizeEEPROM = EEPROM.read(0);                                          //Se lee el tamaño total almacenado de parametros para formar Movimientos

  ejecutando = EEPROM.read(1);                                          //se lee si se estaba ejecutando o no una accion cuando se apago el arduino, y se almacena en variable "ejecutando" 
  cadenaEprom = "";
  for (int i = 2; i <= sizeEEPROM; i++) {                               //se lee byte por byte y se almacena en cadeaEprom separando por comas cada byte, esa cadena será separada en metodo separarCadena(String cadena)

    cadenaEprom += EEPROM.read(i);
    cadenaEprom += ",";

  }
}
