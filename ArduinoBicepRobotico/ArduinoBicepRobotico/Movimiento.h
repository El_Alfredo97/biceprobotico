/*
Esta es una clase que nos permitira crear objetos de tipo Movimiento y manipular de forma mas sencilla la optencion de los pasos a seguir
Cada Movimiento tiene 4  parametros
    1     El eje que indica el servo que se debe de mover
    2     el grado que indica el grado al que se desea mover el eje o servo indicado
    3     la velocidad indica la rapidez con la que se movera el servo al grado indicado
    4     la espera indica el tiempo que se tomara para pasar al siguiente Movimiento

*/


class Movimiento {
    int eje;
    int grado;
    int velocidad;
    int espera;

    //para servos
  public: Movimiento(int ejeIn, int gradoIn, int velocidadIn, int esperaIn) {
      eje = ejeIn;
      grado = gradoIn;
      velocidad = velocidadIn;
      espera = esperaIn;
    }

  public: Movimiento() {

    }

  public: int getEje() {
      return eje;
    }

  public: void setEje(int ejei) {
      eje = ejei;
    }

  public: int getGrado() {
      return grado;
    }

  public: void setGrado(int gradoi) {
      grado = gradoi;
    }

  public: int getEspera() {
      return espera;
    }

  public: void setEspera(int esperai) {
      espera = esperai;
    }

  public: int getVelocidad() {
      return velocidad;
    }

  public: void setVelocidad(int velocidadi) {
      velocidad = velocidadi;
    }

};
