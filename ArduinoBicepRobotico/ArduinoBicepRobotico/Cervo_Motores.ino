
void setupCervos() {
  //se indica que pin corresponde a que servomotor
  pinza.attach(SERVO_PIN_PINZA);
  giroMuneca.attach(SERVO_PIN_GIRO_MUNECA);
  verticalMuneca.attach(SERVO_PIN_VERTICAL_MUNECA);
  codo.attach(SERVO_PIN_CODO);

  //se da una posicion inicial a cada servo motor asi como al motor de pasos
  colocarEnPosicionInicial();

}

void colocarEnPosicionInicial() {
  /*
  Este método se usa para regresar a cada servo a una posición incial
  Cada servoMotor recibe la posicion(grado) hacia el cuál se moverá y la velocidad con la que hara ese movimiento
  */
  moverPinza(MIN_PINZA, 10);
  moverGiroMuneca(10, 10);
  moverMunecaVertical(150, 10);
  moverCodo(120, 30);
  stepMotorMove(4076 / 254 * 50);
}



void loopServos()
{

  EEPROM.write(1, 1);                                               //Se escribe en memoria EEPROM un 1 en la posicion 1, esto para oindicar que se ejecutará algo, en caso apagarse el arduino poder reaunudar


  for (int i = 0; i < sizeMovimientosReal; i++) {                   //se verifica el tamaño total del arreglo de objetos de tipo Movimiento, para cada movimiento tomará sus parametros moverá el servo correspondiente
    //obtencion de parametros del objeto Movimiento actual
    int eje = movimientos[i].getEje();
    int grado = movimientos[i].getGrado();
    int velocidad = movimientos[i].getVelocidad();
    int espera = movimientos[i].getEspera();



    if (ejecutando == 0) {                                            //si mientras se ejecutaban los movimientos la variable "ejecutando" cambia a cero por alguna interrupción, se detiene todo y se sale de este ciclo
      break;
    } else if (ejecutando == 1) {                                     //Si se debe de estar ejecutando algo, se ejecuta el movimeinto de acuerdo a la lista de Movimientos
      showDisplay(4, i, eje, grado);                                  //MUESTRA EN EL DISPLAY EL MENSAJE 4,INDICANDO EL NUMERO DE MOVIMIENTO ACTUAL, EL EJE Y LOS GRADOS A MOVER
                                                                    //Se hace una comparacion para concer que servo se debe mover de acuerdo a los parametros del Movimiento actual
      if (eje == 1) {                                               //si el movimiento actual es la pinza, mueve el servo de la pinza
        moverPinza(grado, velocidad);
        delay(espera);
      } else if (eje == 2) {                                        //si el movimiento actual es el giroMuñeca, mueve el servo de la giroMuñeca
        moverGiroMuneca(grado, velocidad);
        delay(espera);
      } else if (eje == 3) {                                        //si el movimiento actual es el verticalMuñeca, mueve el servo de la verticalMuñeca
        moverMunecaVertical(grado, velocidad);
        delay(espera);
      } else if (eje == 4) {                                        //si el movimiento actual es la codo, mueve el servo de la codo
        moverCodo(grado, velocidad);
        delay(espera);
      } else if (eje == 5) {                                        //si el movimiento actual es la hombro, mueve el servo de la hombro
        motorSpeed = motorSpeed;
        stepMotorMove(4076 / 254 * grado);
        delay(espera);
      }
    }

  }

  EEPROM.write(1, 0);                                               //si todo fue bien, se indica que se termino de ejecutar toda la lista de movimientos almacenada, se guarda en EEPROM


}

//----------------------------------SE MUESTRAN LOS METODOS USADOS PARA MOVER LOS SERVOS YA SEA RECIBIDOS DESDE JAVA O ALMACENADOS EN EL ARDUINO

void moverPinza(int grado, int tiempo) {
/*
  Este método se encarga del movimiento del servo indicado, se valida la posicion actual y el grado al que se debe mover,
  se recibe la velocidad con la que se desea mover a ese grado, por lo que se realiza la validacion de cada grado que se debe de aumentar o disminuir y la velocidad
  */
  
  posicionPinza = pinza.read();                                       //se lee la posicion actual o grado actual en la que se quedo el servo

  //validar movimiento pinza
  if (grado >= MIN_PINZA && grado < MAX_PINZA) {                      //Solo si el grado de movimiento esta entre el rango de grados de movimiento del servo
    if (grado <= posicionPinza) {                                     //si el grado es menor a la posicion actual, se incrementa grado por grado hasta llegar al grado deseado
      for (int i = posicionPinza; i >= grado; i--) {
        pinza.write(i);
        delay(tiempo);
      }
    } else if (grado > posicionPinza) {                               //si el grado es mayor a la posicion actual, se disminuye grado por grado hasta llegar al grado deseado 
      for (int i = posicionPinza; i < grado; i++) {
        pinza.write(i);
        delay(tiempo);
      }
    }
  }


}

void moverGiroMuneca(int grado, int tiempo) {
  /*
  Este método se encarga del movimiento del servo indicado, se valida la posicion actual y el grado al que se debe mover,
  se recibe la velocidad con la que se desea mover a ese grado, por lo que se realiza la validacion de cada grado que se debe de aumentar o disminuir y la velocidad
  */
  
  posicionGiroMuneca = giroMuneca.read();                               //se lee la posicion actual o grado actual en la que se quedo el servo


  //validar movimiento pinza
  if (grado >= MIN_GIRO_MUNECA && grado < MAX_GIRO_MUNECA) {            //Solo si el grado de movimiento esta entre el rango de grados de movimiento del servo
    if (grado <= posicionGiroMuneca) {                                  //si el grado es menor a la posicion actual, se incrementa grado por grado hasta llegar al grado deseado
      for (int i = posicionGiroMuneca; i >= grado; i--) {
        giroMuneca.write(i);
        delay(tiempo);
      }
    } else if (grado > posicionGiroMuneca) {                            //si el grado es mayor a la posicion actual, se decremetna grado por grado hasta llegar al grado deseado
      for (int i = posicionGiroMuneca; i < grado; i++) {
        giroMuneca.write(i);
        delay(tiempo);
      }
    }
  }

}

void moverMunecaVertical(int grado, int tiempo) {
  /*
  Este método se encarga del movimiento del servo indicado, se valida la posicion actual y el grado al que se debe mover,
  se recibe la velocidad con la que se desea mover a ese grado, por lo que se realiza la validacion de cada grado que se debe de aumentar o disminuir y la velocidad
  */
  
  posicionVerticalMuneca = verticalMuneca.read();                        //se lee la posicion actual o grado actual en la que se quedo el servo

  //validar movimiento pinza
  if (grado >= MIN_VERTICAL_MUNECA && grado < MAX_VERTICAL_MUNECA) {    //Solo si el grado de movimiento esta entre el rango de grados de movimiento del servo
    if (grado <= posicionVerticalMuneca) {                              //si el grado es menor a la posicion actual, se incrementa grado por grado hasta llegar al grado deseado
      for (int i = posicionVerticalMuneca; i >= grado; i--) {
        verticalMuneca.write(i);
        delay(tiempo);
      }
    } else if (grado > posicionVerticalMuneca) {                        //si el grado es mayor a la posicion actual, se decremetna grado por grado hasta llegar al grado deseado
      for (int i = posicionVerticalMuneca; i < grado; i++) {
        verticalMuneca.write(i);
        delay(tiempo);
      }
    }
  }
}

void moverCodo(int grado, int tiempo) {
  /*
  Este método se encarga del movimiento del servo indicado, se valida la posicion actual y el grado al que se debe mover,
  se recibe la velocidad con la que se desea mover a ese grado, por lo que se realiza la validacion de cada grado que se debe de aumentar o disminuir y la velocidad
  */
  
  posicionCodo = codo.read();                                             //se lee la posicion actual o grado actual en la que se quedo el servo

  //validar movimiento pinza
  if (grado >= MIN_CODO && grado < MAX_CODO) {                            //Solo si el grado de movimiento esta entre el rango de grados de movimiento del servo
    if (grado <= posicionCodo) {                                          //si el grado es menor a la posicion actual, se incrementa grado por grado hasta llegar al grado deseado
      for (int i = posicionCodo; i >= grado; i--) {
        codo.write(i);
        delay(tiempo);
      }
    } else if (grado > posicionCodo) {                                    //si el grado es mayor a la posicion actual, se decremetna grado por grado hasta llegar al grado deseado
      for (int i = posicionCodo; i < grado; i++) {
        codo.write(i);
        delay(tiempo);
      }
    }
  }
}


//----------------------------------A PARTIR DE AQUI, SE MUESTRAN LOS METODOS USADOS PARA MOVER LOS SERVOS DE ACUERDO A LOS VALORES RECIBIDOS DESDE LA APLICACION DE ARDUINO


void moverPinzaRel(int direccion) {                                       //solo se recibe la direccion a la que se desea mover y se incrementa o decrementa el grado
  
  int pos = pinza.read();
  if (direccion == 0) {
    if (pos < MAX_PINZA) {
      pos++;
      pinza.write(pos);
    }
  } else {
    if (pos > MIN_PINZA) {
      pos--;
      pinza.write(pos);
    }
  }
}

void moverGiroMunecaRel(int direccion) {                                       //solo se recibe la direccion a la que se desea mover y se incrementa o decrementa el grado
  int pos = giroMuneca.read();
  if (direccion == 0) {
    if (pos < MAX_GIRO_MUNECA) {
      pos++;
      giroMuneca.write(pos);
    }
  } else {
    if (pos > MIN_GIRO_MUNECA) {
      pos--;
      giroMuneca.write(pos);
    }
  }

}

void moverMunecaVerticalRel(int direccion) {                                       //solo se recibe la direccion a la que se desea mover y se incrementa o decrementa el grado
  int pos = verticalMuneca.read();
  if (direccion == 0) {
    if (pos < MAX_VERTICAL_MUNECA) {
      pos++;
      verticalMuneca.write(pos);
    }
  } else {
    if (pos > MIN_VERTICAL_MUNECA) {
      pos--;
      verticalMuneca.write(pos);
    }
  }
}

void moverCodoRel(int direccion) {                                       //solo se recibe la direccion a la que se desea mover y se incrementa o decrementa el grado
  int pos = codo.read();
  if (direccion == 0) {
    if (pos < MAX_CODO) {
      pos++;
      codo.write(pos);
    }
  } else {
    if (pos > MIN_CODO) {
      pos--;
      codo.write(pos);
    }
  }
}

void moverHombroRel(int direccion) {                                       //solo se recibe la direccion a la que se desea mover y se incrementa o decrementa el grado
  int pos = positionCont;
  if (direccion == 0) {
    if (pos < positionCont) {
      pos = pos + 200;
      stepMotorMove(pos);
    }
  } else {
    if (pos > positionCont) {
      pos = pos - 200;
      stepMotorMove(pos);
    }
  }
}
