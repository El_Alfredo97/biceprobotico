package vista;

//Importación de librerias necesarias para los componentes 
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.table.DefaultTableModel;

public class PanelInstrucciones extends JPanel {

    //Declaración de componentes
    private JTable tabla;
    private DefaultTableModel dtm;
    String columnas[] = {"Articulación", "Movimiento (en grados)", "Velocidad", "Espera"};
    private GridBagLayout esquema;
    private GridBagConstraints restricciones;
    JPanel panelVistaInstrucciones, panelBotones;
    private JLabel lblHombro, lblMuñecaVertical, lblCodo, lblMuñecaGiro, lblPinza, lblEspacio, lblEspera, lblVelocidad, lblTitulo1, lblTitulo2;
    private JTextField tfBase, txtEsperaHombro, txtVelocidadHombro, txtEsperaCodo, txtVelocidadCodo, txtEsperaMuñecaVertical, txtVelocidadMuñecaVertical, txtEsperaMuñecaGiro, txtVelocidadMuñecaGiro, txtEsperaPinza, txtVelocidadPinza;
    private JButton btnHombro, btnMuñecaVertical, btnCodo, btnMuñecaGiro, btnPinza, btnEnviarArduino, btnBorrarPaso, btnGuardar, btnParo;
    private JSlider jsHombro, jsMuñecaVertical, jsCodo, jsMuñecaGiro, jsPinza;
    private JPanel panelSuperior;

    public PanelInstrucciones() {
        initComponents();
        //setLayout(new BorderLayout());
    }

    public void initComponents() {

        panelSuperior = new JPanel(new BorderLayout());
        panelBotones = new JPanel();
        add(panelSuperior, BorderLayout.NORTH);

        //Creación de la tabla y el modelo a utilizar en ella
        dtm = new DefaultTableModel(null, columnas);
        tabla = new JTable(dtm);
        //Dimensiones de la tabla
        tabla.setPreferredScrollableViewportSize(new Dimension(500, 400));
        JScrollPane scroll = new JScrollPane(tabla);
        panelSuperior.add(scroll, BorderLayout.CENTER);

        //Creación del panel que contiene los controles,con 
        //un gestor de esquema de tipo GridBagLayout.
        panelVistaInstrucciones = new JPanel();
        esquema = new GridBagLayout();
        //establece el esquema del marco
        panelVistaInstrucciones.setLayout(esquema);
        restricciones = new GridBagConstraints();
        restricciones.anchor = GridBagConstraints.WEST;

        //Se crean los componentes Label
        lblTitulo1 = new JLabel("Velocidad");
        lblTitulo2 = new JLabel("Espera");
        lblHombro = new JLabel("Hombro:");
        lblCodo = new JLabel("Codo:");
        lblMuñecaVertical = new JLabel("Muñeca vertical:");
        lblMuñecaGiro = new JLabel("Muñeca giro:");
        lblPinza = new JLabel("Pinza:");
        lblEspacio = new JLabel("   ");

        //Se crean los componentes JTextfield, pasandole como parametro un texto default correspondiente a la velocidad, el 2do parametro
        //corresponde al tamaño del componente
        txtEsperaHombro = new JTextField("50", 8);
        txtVelocidadHombro = new JTextField("20", 8);
        txtEsperaCodo = new JTextField("50", 8);
        txtVelocidadCodo = new JTextField("20", 8);
        txtEsperaMuñecaVertical = new JTextField("50", 8);
        txtVelocidadMuñecaVertical = new JTextField("20", 8);
        txtEsperaMuñecaGiro = new JTextField("50", 8);
        txtVelocidadMuñecaGiro = new JTextField("20", 8);
        txtEsperaPinza = new JTextField("50", 8);
        txtVelocidadPinza = new JTextField("20", 8);

        //Se crean los componentes JButton, se les añade color de fondo y color de texto
        btnHombro = new JButton("Añadir");
        btnCodo = new JButton("Añadir");
        btnMuñecaVertical = new JButton("Añadir");
        btnMuñecaGiro = new JButton("Añadir");
        btnPinza = new JButton("Añadir");
        btnEnviarArduino = new JButton("Enviar");
        btnEnviarArduino.setBackground(new java.awt.Color(9, 197, 249));
        btnEnviarArduino.setForeground(Color.WHITE);
        btnParo = new JButton("Paro de emergencia");
        btnParo.setBackground(new java.awt.Color(240, 92, 48));
        btnParo.setForeground(Color.WHITE);
        btnGuardar = new JButton("Guardar");
        btnGuardar.setBackground(new java.awt.Color(89, 204, 15));
        btnGuardar.setForeground(Color.WHITE);
        btnBorrarPaso = new JButton("Borrar paso");
        btnBorrarPaso.setBackground(new java.awt.Color(195, 224, 94));
        btnBorrarPaso.setForeground(Color.BLACK);

        //Se crean los componentes JSLider, se carga el constructor que recibe 4 parametros correspondientes a (posición vertical,valor incial,valor final, posicion donde comienza al iniciar programa)
        jsHombro = new JSlider(JSlider.HORIZONTAL, 0, 254, 120);
        jsCodo = new JSlider(JSlider.HORIZONTAL, 0, 180, 120);
        jsMuñecaVertical = new JSlider(JSlider.HORIZONTAL, 30, 180, 140);
        jsMuñecaGiro = new JSlider(JSlider.HORIZONTAL, 0, 180, 20);
        jsPinza = new JSlider(JSlider.HORIZONTAL, 36, 137, 40);

        //se invierte el relleno del JSlider (desde donde comienza)
        jsHombro.setInverted(false);
        //indicadores que marcan los números
        jsHombro.setPaintTicks(true);
        //Valor de cuanto en cuanto se mesutran los numeros en el componente
        jsHombro.setMajorTickSpacing(60);
        //De cuanto en cuanto mostrar los indicadores
        jsHombro.setMinorTickSpacing(10);
        //Permitir visulizar los numeros en el componente
        jsHombro.setPaintLabels(true);

        //Ocurre la misma descripcion previa para cada JSlider
        jsCodo.setInverted(false);
        jsCodo.setPaintTicks(true);
        jsCodo.setMajorTickSpacing(30);
        jsCodo.setMinorTickSpacing(5);
        jsCodo.setPaintLabels(true);

        jsMuñecaVertical.setInverted(false);
        jsMuñecaVertical.setPaintTicks(true);
        jsMuñecaVertical.setMajorTickSpacing(30);
        jsMuñecaVertical.setMinorTickSpacing(5);
        jsMuñecaVertical.setPaintLabels(true);

        jsMuñecaGiro.setInverted(false);
        jsMuñecaGiro.setPaintTicks(true);
        jsMuñecaGiro.setMajorTickSpacing(30);
        jsMuñecaGiro.setMinorTickSpacing(5);
        jsMuñecaGiro.setPaintLabels(true);

        jsPinza.setInverted(false);
        jsPinza.setPaintTicks(true);
        jsPinza.setMajorTickSpacing(30);
        jsPinza.setMinorTickSpacing(5);
        jsPinza.setPaintLabels(true);

        //Metodo para agregar componentes al panel
        //Agregar los titulos
        agregarComponente(lblTitulo1, 0, 5, 1, 1);
        agregarComponente(lblTitulo2, 0, 9, 1, 1);
        agregarComponente(lblHombro, 1, 0, 1, 1);
        agregarComponente(lblCodo, 2, 0, 1, 1);
        agregarComponente(lblMuñecaVertical, 3, 0, 1, 1);
        agregarComponente(lblMuñecaGiro, 4, 0, 1, 1);
        agregarComponente(lblPinza, 5, 0, 1, 1);

        //Agregar los JSlider
        agregarComponente(lblEspacio, 1, 6, 1, 1);
        agregarComponente(jsHombro, 1, 1, 3, 1);
        agregarComponente(jsCodo, 2, 1, 3, 1);
        agregarComponente(jsMuñecaVertical, 3, 1, 3, 1);
        agregarComponente(jsMuñecaGiro, 4, 1, 3, 1);
        agregarComponente(jsPinza, 5, 1, 3, 1);

        //Agregar los JTextField
        agregarComponente(txtVelocidadHombro, 1, 5, 1, 1);
        agregarComponente(txtEsperaHombro, 1, 9, 1, 1);
        agregarComponente(txtVelocidadCodo, 2, 5, 1, 1);
        agregarComponente(txtEsperaCodo, 2, 9, 1, 1);
        agregarComponente(txtVelocidadMuñecaVertical, 3, 5, 1, 1);
        agregarComponente(txtEsperaMuñecaVertical, 3, 9, 1, 1);
        agregarComponente(txtVelocidadMuñecaGiro, 4, 5, 1, 1);
        agregarComponente(txtEsperaMuñecaGiro, 4, 9, 1, 1);
        agregarComponente(txtVelocidadPinza, 5, 5, 1, 1);
        agregarComponente(txtEsperaPinza, 5, 9, 1, 1);
        agregarComponente(btnHombro, 1, 10, 1, 1);

        //Agregar los botones de cada eje
        agregarComponente(btnCodo, 2, 10, 1, 1);
        agregarComponente(btnMuñecaVertical, 3, 10, 1, 1);
        agregarComponente(btnMuñecaGiro, 4, 10, 1, 1);
        agregarComponente(btnPinza, 5, 10, 1, 1);

        //AGregamos los botones de guardar, borrar paso, enviar y paro, a un panel inferior
        panelSuperior.add(panelVistaInstrucciones, BorderLayout.WEST);
        panelBotones.add(btnGuardar);
        panelBotones.add(btnBorrarPaso);
        panelBotones.add(btnEnviarArduino);
        panelBotones.add(btnParo);
        panelSuperior.add(panelBotones, BorderLayout.SOUTH);
    }

    //Metodo para agregar componentes
    private void agregarComponente(Component componente,
        int fila, int columna, int anchura, int altura) {
        restricciones.gridx = columna; // establece gridx
        restricciones.gridy = fila; // establece gridy
        restricciones.gridwidth = anchura; // establece gridwidth
        restricciones.gridheight = altura; // establece gridheight
        esquema.setConstraints(componente, restricciones);//establece restricciones
        panelVistaInstrucciones.add(componente); // agrega el componente
    }

    //Getters y setters de cada componente 
    public JTable getTabla() {
        return tabla;
    }

    public void setTabla(JTable tabla) {
        this.tabla = tabla;
    }

    public DefaultTableModel getDtm() {
        return dtm;
    }

    public void setDtm(DefaultTableModel dtm) {
        this.dtm = dtm;
    }

    public JTextField getTfBase() {
        return tfBase;
    }

    public void setTfBase(JTextField tfBase) {
        this.tfBase = tfBase;
    }

    public JButton getBtnHombro() {
        return btnHombro;
    }

    public void setBtnHombro(JButton btnHombro) {
        this.btnHombro = btnHombro;
    }

    public JButton getBtnEnviarArduino() {
        return btnEnviarArduino;
    }

    public void setBtnEnviarArduino(JButton btnEnviarArduino) {
        this.btnEnviarArduino = btnEnviarArduino;
    }

    public JSlider getJsHombro() {
        return jsHombro;
    }

    public void setJsHombro(JSlider jsHombro) {
        this.jsHombro = jsHombro;
    }

    public JSlider getJsMuñecaVertical() {
        return jsMuñecaVertical;
    }

    public void setJsMuñecaVertical(JSlider jsMuñecaVertical) {
        this.jsMuñecaVertical = jsMuñecaVertical;
    }

    public JSlider getJsCodo() {
        return jsCodo;
    }

    public void setJsCodo(JSlider jsCodo) {
        this.jsCodo = jsCodo;
    }

    public JSlider getJsMuñecaGiro() {
        return jsMuñecaGiro;
    }

    public void setJsMuñecaGiro(JSlider jsMuñecaGiro) {
        this.jsMuñecaGiro = jsMuñecaGiro;
    }

    public JSlider getJsPinza() {
        return jsPinza;
    }

    public void setJsPinza(JSlider jsPinza) {
        this.jsPinza = jsPinza;
    }

    public JTextField getTxtEsperaHombro() {
        return txtEsperaHombro;
    }

    public void setTxtEsperaHombro(JTextField txtEsperaHombro) {
        this.txtEsperaHombro = txtEsperaHombro;
    }

    public JTextField getTxtVelocidadHombro() {
        return txtVelocidadHombro;
    }

    public void setTxtVelocidadHombro(JTextField txtVelocidadHombro) {
        this.txtVelocidadHombro = txtVelocidadHombro;
    }

    public JTextField getTxtEsperaCodo() {
        return txtEsperaCodo;
    }

    public void setTxtEsperaCodo(JTextField txtEsperaCodo) {
        this.txtEsperaCodo = txtEsperaCodo;
    }

    public JTextField getTxtVelocidadCodo() {
        return txtVelocidadCodo;
    }

    public void setTxtVelocidadCodo(JTextField txtVelocidadCodo) {
        this.txtVelocidadCodo = txtVelocidadCodo;
    }

    public JTextField getTxtEsperaMuñecaVertical() {
        return txtEsperaMuñecaVertical;
    }

    public void setTxtEsperaMuñecaVertical(JTextField txtEsperaMuñecaVertical) {
        this.txtEsperaMuñecaVertical = txtEsperaMuñecaVertical;
    }

    public JTextField getTxtVelocidadMuñecaVertical() {
        return txtVelocidadMuñecaVertical;
    }

    public void setTxtVelocidadMuñecaVertical(JTextField txtVelocidadMuñecaVertical) {
        this.txtVelocidadMuñecaVertical = txtVelocidadMuñecaVertical;
    }

    public JTextField getTxtEsperaMuñecaGiro() {
        return txtEsperaMuñecaGiro;
    }

    public void setTxtEsperaMuñecaGiro(JTextField txtEsperaMuñecaGiro) {
        this.txtEsperaMuñecaGiro = txtEsperaMuñecaGiro;
    }

    public JTextField getTxtVelocidadMuñecaGiro() {
        return txtVelocidadMuñecaGiro;
    }

    public void setTxtVelocidadMuñecaGiro(JTextField txtVelocidadMuñecaGiro) {
        this.txtVelocidadMuñecaGiro = txtVelocidadMuñecaGiro;
    }

    public JTextField getTxtEsperaPinza() {
        return txtEsperaPinza;
    }

    public void setTxtEsperaPinza(JTextField txtEsperaPinza) {
        this.txtEsperaPinza = txtEsperaPinza;
    }

    public JTextField getTxtVelocidadPinza() {
        return txtVelocidadPinza;
    }

    public void setTxtVelocidadPinza(JTextField txtVelocidadPinza) {
        this.txtVelocidadPinza = txtVelocidadPinza;
    }

    public JButton getBtnMuñecaVertical() {
        return btnMuñecaVertical;
    }

    public void setBtnMuñecaVertical(JButton btnMuñecaVertical) {
        this.btnMuñecaVertical = btnMuñecaVertical;
    }

    public JButton getBtnCodo() {
        return btnCodo;
    }

    public void setBtnCodo(JButton btnCodo) {
        this.btnCodo = btnCodo;
    }

    public JButton getBtnMuñecaGiro() {
        return btnMuñecaGiro;
    }

    public void setBtnMuñecaGiro(JButton btnMuñecaGiro) {
        this.btnMuñecaGiro = btnMuñecaGiro;
    }

    public JButton getBtnPinza() {
        return btnPinza;
    }

    public void setBtnPinza(JButton btnPinza) {
        this.btnPinza = btnPinza;
    }

    public JButton getBtnBorrarPaso() {
        return btnBorrarPaso;
    }

    public void setBtnBorrarPaso(JButton btnBorrarPaso) {
        this.btnBorrarPaso = btnBorrarPaso;
    }

    public JButton getBtnGuardar() {
        return btnGuardar;
    }

    public void setBtnGuardar(JButton btnGuardar) {
        this.btnGuardar = btnGuardar;
    }

    public JButton getBtnParo() {
        return btnParo;
    }

    public void setBtnParo(JButton btnParo) {
        this.btnParo = btnParo;
    }

}
