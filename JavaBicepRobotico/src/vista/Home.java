//Paquete de la clase
package vista;

//Importación de librerias necesarias para los componentes y acceso a clases externas
import Controlador.ControladorInstrucciones;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class Home extends JFrame {

    //Declaración de componentes
    private JDesktopPane escritorioPadre;
    JMenuItem menuAbrir;

    //Constructor
    public Home() {
        super("Panel de control");
        // cambiar la apariencia visual
        try {
            // establece la apariencia visual para esta aplicación
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
            // actualiza los componentes en esta aplicación
            SwingUtilities.updateComponentTreeUI(this);
        } catch (Exception excepcion) {
            excepcion.printStackTrace();
        } // fin de catch

        //Inicializacion de componentes del menú
        JMenuBar barra = new JMenuBar(); // crea la barra de menús
        JMenu menuPrincipal = new JMenu("Archivo"); // crea el menú Archivo
        JMenu menuVer = new JMenu("Programación");
        menuAbrir = new JMenuItem("Abrir archivo");
        menuAbrir.setVisible(false);

        //Se agregan los items del menú a su correspondiente JMenu
        JMenuItem jmiProgramacion = new JMenuItem("Añadir instrucciones");
        menuVer.add(jmiProgramacion);
        menuPrincipal.add(menuVer);
        menuPrincipal.add(menuAbrir);
        barra.add(menuPrincipal); // agrega el menú Agregar a la barra de menús
        setJMenuBar(barra); // establece la barra de menús para esta aplicación


        escritorioPadre = new JDesktopPane(); // crea el panel de escritorio
        add(escritorioPadre); // agrega el panel de escritorio al marco

        // establece componente de escucha para el elemento de menú 
        jmiProgramacion.addActionListener(
                new ActionListener() // clase interna anónima
        {
            // muestra la nueva ventana interna
            public void actionPerformed(ActionEvent evento) {
                // crea el marco interno
                JInternalFrame marco = new JInternalFrame("Panel de control", true, true, true, true);
                //Instancia de panel que contendrá los componentes visuales de instrucciones
                PanelInstrucciones panelInstruccion = new PanelInstrucciones();
                marco.add(panelInstruccion, BorderLayout.CENTER); // agrega el panel

                marco.addInternalFrameListener(new ControladorInstrucciones(panelInstruccion, Home.this));//Se le agrega un listener al marco
                marco.pack(); // establece marco interno al tamaño del contenido
                marco.setSize(1150, 525);
                escritorioPadre.add(marco); // adjunta marco interno

                marco.setVisible(true); // muestra marco interno 
                marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            }
        } // fin de la clase interna anónima
        ); // fin de la llamada a addActionListener

     
    }

    public JMenuItem getMenuAbrir() {
        return menuAbrir;
    }

    public void setMenuAbrir(JMenuItem menuAbrir) {
        this.menuAbrir = menuAbrir;
    }

}
