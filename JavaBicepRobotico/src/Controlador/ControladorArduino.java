//Paquete de la clase
package Controlador;

//Importación de librerias necesarias para los componentes y acceso a clases externas
import com.panamahitek.ArduinoException;
import com.panamahitek.PanamaHitek_Arduino;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import java.io.UnsupportedEncodingException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import jssc.SerialPort;
import jssc.SerialPortException;

public class ControladorArduino {

    //Variables comodines
    String valor = "";
    private boolean quiereMensaje = false;
    int valorTecla;

    PanamaHitek_Arduino Arduino = new PanamaHitek_Arduino();

    //Verificacion de conectividad con el puerto serial, mostrando el estado de la conexión
    jssc.SerialPortEventListener eventoArduino = new jssc.SerialPortEventListener() {
        @Override
        public void serialEvent(jssc.SerialPortEvent spe) {

            try {
                if (Arduino.isMessageAvailable()) {
                    System.out.println(Arduino.printMessage());
                }
            } catch (SerialPortException ex) {
                Logger.getLogger(ControladorArduino.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ArduinoException ex) {
                Logger.getLogger(ControladorArduino.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    };

    //Constructor de la clase, se especifica el puerto a utilizar para la conexión
    public ControladorArduino() {

        try {

            Arduino.arduinoRXTX("/dev/ttyACM0", 9600, eventoArduino);
            //Arduino.arduinoTX("/dev/ttyACM0", 9600);

            //Arduino.arduinoRXTX("/dev/ttyACM1", 9600, eventoArduino);
            //Arduino.arduinoTX("/dev/ttyACM0", 9600);


        } catch (ArduinoException ex) {
            Logger.getLogger(ControladorArduino.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }

    }
        //Metodo clave para realizar la conexión Java-Arduino, recive como parametro un identificador, que, dependiendo su valor será la acción a realizar con los datos que 
        //se envian
        public void enviarMensaje(int opt, int data) {

        try {
            Arduino.sendByte(opt);
            Arduino.sendByte(data);
        } catch (ArduinoException ex) {
            Logger.getLogger(ControladorArduino.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SerialPortException ex) {
            Logger.getLogger(ControladorArduino.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    void enviarCadena(int opt, String data) {
        try {
            Arduino.sendByte(opt);
            Arduino.sendByte(opt);
            Arduino.sendData(data);
        } catch (ArduinoException ex) {
            Logger.getLogger(ControladorArduino.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SerialPortException ex) {
            Logger.getLogger(ControladorArduino.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void enviarTipo(String data) {
        try {
            Arduino.sendData(data);
        } catch (ArduinoException ex) {
            Logger.getLogger(ControladorArduino.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SerialPortException ex) {
            Logger.getLogger(ControladorArduino.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Geeters y setters de la clase 
    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public int getValorTecla() {
        return valorTecla;
    }

    public void setValorTecla(int valorTecla) {
        this.valorTecla = valorTecla;
    }

}
