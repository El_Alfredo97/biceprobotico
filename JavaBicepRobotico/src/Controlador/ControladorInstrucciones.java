//Paquete de la clase
package Controlador;

//Importación de librerias necesarias para los componentes y acceso a clases externas
import Modelo.Movimiento;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import vista.Home;
import vista.PanelInstrucciones;
import vista.PanelInstrucciones;

//Clase que hereda de InternalFrameAdapter e implementa eventos tipo Listener
public class ControladorInstrucciones extends InternalFrameAdapter implements ActionListener, ChangeListener, MouseListener {

    //Declaración de los componentes locales y clases externas a utilizar
    private PanelInstrucciones accesoControles;
    private Home accesoBarra;
    private ControladorArduino arduino;
    private int filaSelec;
    private ArrayList<Modelo.Movimiento> movimientos;
    private String ruta = "src/HistorialInstrucciones/";
    Movimiento mov;

    //La clase controladora requiere un panel y un controlador de arduino para funcionar, pues dota a la
    //interfaz grafica de realizar acciones o eventos bajo determinadas circunstancias.
    public ControladorInstrucciones(PanelInstrucciones accesoControles, Home accesoBarra) {
        this.accesoControles = accesoControles;
        this.accesoBarra = accesoBarra;
        arduino = new ControladorArduino();
        movimientos = new ArrayList();

    }

    //Sobrecarga de constructor
    public ControladorInstrucciones(PanelInstrucciones accesoControles) {
        movimientos = new ArrayList<>();
        this.accesoControles = accesoControles;
        arduino = new ControladorArduino();

    }

    //Al abrir una ventana de tipo JInternalFrame se dota para estar a la escucha y realizar eventos establecidos.
    public void internalFrameOpened(InternalFrameEvent e) {

        //Al ser una clase con Listeners agregamos los listener a todos los componentes necesarios
        accesoControles.getBtnHombro().addActionListener(this);
        accesoControles.getBtnCodo().addActionListener(this);
        accesoControles.getBtnMuñecaVertical().addActionListener(this);
        accesoControles.getBtnMuñecaGiro().addActionListener(this);
        accesoControles.getBtnPinza().addActionListener(this);
        accesoControles.getBtnEnviarArduino().addActionListener(this);
        accesoControles.getJsHombro().addMouseListener(this);
        accesoControles.getJsCodo().addChangeListener(this);
        accesoControles.getJsMuñecaVertical().addChangeListener(this);
        accesoControles.getJsMuñecaGiro().addChangeListener(this);
        accesoControles.getJsPinza().addChangeListener(this);
        accesoControles.getBtnBorrarPaso().addActionListener(this);
        accesoControles.getBtnGuardar().addActionListener(this);
        accesoControles.getBtnParo().addActionListener(this);
        accesoBarra.getMenuAbrir().setVisible(true);
        accesoBarra.getMenuAbrir().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //Variables comodines.
        //El eje se almacena en un valor int, dependiendo el eje la asignacion quedaria: Pinza=1 , Giro Muñeca =2, Muñeca vertical= 3, Codo = 4, Hombro = 5.
        int eje;
        //Guarda el grado establecido del eje correspondiente
        int grado;
        //Velocidad y espera del eje correpondiente
        int velocidad;
        int espera;

                                                        //Condicional para determinar cual boton fue presionado
        //boton Añadir movimiento de hombro
        if (e.getSource() == accesoControles.getBtnHombro()) {

            //Se toman los valores como eje seleccionado, el grado de movimiento, la velocidad y espera. Estos valores se agregan al componente JTable
            accesoControles.getDtm().addRow(new Object[]{"Hombro", accesoControles.getJsHombro().getValue(), accesoControles.getTxtVelocidadHombro().getText(), accesoControles.getTxtEsperaHombro().getText()});

            //Asignacion de variables comodines para el eje hombro
            eje = 5;
            grado = accesoControles.getJsHombro().getValue();
            velocidad = Integer.parseInt(accesoControles.getTxtVelocidadHombro().getText());
            espera = Integer.parseInt(accesoControles.getTxtEsperaHombro().getText());

            //Se crea un objeto con las variables anteriores
            Movimiento m = new Movimiento(eje, grado, velocidad, espera);
            //Se agrega el objeto creado a un ArrayList de objetos
            movimientos.add(m);

            
            //boton Añadir movimiento de codo
        } else if (e.getSource() == accesoControles.getBtnCodo()) {
            //Se toman los valores como eje seleccionado, el grado de movimiento, la velocidad y espera. Estos valores se agregan al componente JTable
            accesoControles.getDtm().addRow(new Object[]{"Codo", accesoControles.getJsCodo().getValue(), accesoControles.getTxtVelocidadCodo().getText(), accesoControles.getTxtEsperaCodo().getText()});

            //Asignacion de variables comodines para el eje codo
            eje = 4;
            grado = accesoControles.getJsCodo().getValue();
            velocidad = Integer.parseInt(accesoControles.getTxtVelocidadCodo().getText());
            espera = Integer.parseInt(accesoControles.getTxtEsperaCodo().getText());

            //Se crea un objeto con las variables anteriores
            Movimiento m = new Movimiento(eje, grado, velocidad, espera);
            //Se agrega el objeto creado a un ArrayList de objetos
            movimientos.add(m);

            
            //boton Añadir movimiento de muñeca vertical
        } else if (e.getSource() == accesoControles.getBtnMuñecaVertical()) {
            //Se toman los valores como eje seleccionado, el grado de movimiento, la velocidad y espera. Estos valores se agregan al componente JTable
            accesoControles.getDtm().addRow(new Object[]{"Muñeca vertical", accesoControles.getJsMuñecaVertical().getValue(), accesoControles.getTxtVelocidadMuñecaVertical().getText(), accesoControles.getTxtEsperaMuñecaVertical().getText()});

            //Asignacion de variables comodines para el eje de muñeca vertical
            eje = 3;
            grado = accesoControles.getJsMuñecaVertical().getValue();
            velocidad = Integer.parseInt(accesoControles.getTxtVelocidadMuñecaVertical().getText());
            espera = Integer.parseInt(accesoControles.getTxtEsperaMuñecaVertical().getText());
            
            //Se crea un objeto con las variables anteriores
            Movimiento m = new Movimiento(eje, grado, velocidad, espera);
            //Se agrega el objeto creado a un ArrayList de objetos
            movimientos.add(m);

            
            //boton Añadir movimiento de giro de muñeca 
        } else if (e.getSource() == accesoControles.getBtnMuñecaGiro()) {
            //Se toman los valores como eje seleccionado, el grado de movimiento, la velocidad y espera. Estos valores se agregan al componente JTable
            accesoControles.getDtm().addRow(new Object[]{"Giro de muñeca", accesoControles.getJsMuñecaGiro().getValue(), accesoControles.getTxtVelocidadMuñecaGiro().getText(), accesoControles.getTxtEsperaMuñecaGiro().getText()});

            //Asignacion de variables comodines para el eje de giro de muñeca
            eje = 2;
            grado = accesoControles.getJsMuñecaGiro().getValue();
            velocidad = Integer.parseInt(accesoControles.getTxtVelocidadMuñecaGiro().getText());
            espera = Integer.parseInt(accesoControles.getTxtEsperaMuñecaGiro().getText());

            //Se crea un objeto con las variables anteriores
            Movimiento m = new Movimiento(eje, grado, velocidad, espera);

            //Se agrega el objeto creado a un ArrayList de objetos
            movimientos.add(m);

            
            //boton Añadir movimiento de las pinzas
        } else if (e.getSource() == accesoControles.getBtnPinza()) {
            //Se toman los valores como eje seleccionado, el grado de movimiento, la velocidad y espera. Estos valores se agregan al componente JTable
            accesoControles.getDtm().addRow(new Object[]{"Pinza", accesoControles.getJsPinza().getValue(), accesoControles.getTxtVelocidadPinza().getText(), accesoControles.getTxtEsperaPinza().getText()});

            //Asignacion de variables comodines para el eje de pinzas
            eje = 1;
            grado = accesoControles.getJsPinza().getValue();
            velocidad = Integer.parseInt(accesoControles.getTxtVelocidadPinza().getText());
            espera = Integer.parseInt(accesoControles.getTxtEsperaPinza().getText());

            //Se crea un objeto con las variables anteriores
            Movimiento m = new Movimiento(eje, grado, velocidad, espera);

            //Se agrega el objeto creado a un ArrayList de objetos
            movimientos.add(m);

            
          //Boton para borrar algun paso previamente registrado en el JTable  
        } else if (e.getSource() == accesoControles.getBtnBorrarPaso()) {
            //Se obtiene la fila seleccionada y se verifica que sea >= 0 
            filaSelec = accesoControles.getTabla().getSelectedRow();
            if (filaSelec >= 0) {
                
                //Borramos la fila y actualizamos la tabla
                accesoControles.getDtm().removeRow(accesoControles.getTabla().getSelectedRow());
                accesoControles.getDtm().fireTableDataChanged();
            }
            
          //Boton para guardar en un archivo txt los pasos establecidos en el JTable   
        } else if (e.getSource() == accesoControles.getBtnGuardar()) {
           
            String archivo = JOptionPane.showInputDialog(null, "Ingrese el nombre del archivo a guardar", "Guardar archivo", JOptionPane.INFORMATION_MESSAGE);
            //lamada al metodo que escribe el archivo
            escribirArchivo(archivo);
            
            //Boton correspondiente a un paro de emergencia
        } else if (e.getSource() == accesoControles.getBtnParo()) {
            JOptionPane.showMessageDialog(null, "Paro de emergencia", "Fallo de sistema", JOptionPane.ERROR_MESSAGE);
            
          //Evento para el JMenuitem, al ser seleccionado permite abrir un archivo previamente guardado  
        } else if (e.getSource() == accesoBarra.getMenuAbrir()) {
            leerPrograma();
            actualizarTabla();

            
           // accesoControles.getDtm().addRow(new Object[]{"Hombro", accesoControles.getJsHombro().getValue(), accesoControles.getTxtVelocidadHombro().getText(), accesoControles.getTxtEsperaHombro().getText()});
       
        
        } else if (e.getSource() == accesoControles.getBtnCodo()) {
            accesoControles.getDtm().addRow(new Object[]{"Codo", accesoControles.getJsCodo().getValue(), accesoControles.getTxtVelocidadCodo().getText(), accesoControles.getTxtEsperaCodo().getText()});
        } else if (e.getSource() == accesoControles.getBtnMuñecaVertical()) {
            accesoControles.getDtm().addRow(new Object[]{"Muñeca vertical", accesoControles.getJsMuñecaVertical().getValue(), accesoControles.getTxtVelocidadMuñecaVertical().getText(), accesoControles.getTxtEsperaMuñecaVertical().getText()});
        } else if (e.getSource() == accesoControles.getBtnMuñecaGiro()) {
            accesoControles.getDtm().addRow(new Object[]{"Giro de muñeca", accesoControles.getJsMuñecaGiro().getValue(), accesoControles.getTxtVelocidadMuñecaGiro().getText(), accesoControles.getTxtEsperaMuñecaGiro().getText()});
        } else if (e.getSource() == accesoControles.getBtnPinza()) {
            accesoControles.getDtm().addRow(new Object[]{"Pinza", accesoControles.getJsPinza().getValue(), accesoControles.getTxtVelocidadPinza().getText(), accesoControles.getTxtEsperaPinza().getText()});
            
            //Boton para enviar los pasos programados a arduino
        } else if (e.getSource() == accesoControles.getBtnEnviarArduino()) {
           //Variable comodin
            String movimientos = "";
            //Se recorre todas las columnas de la tabla
            for (int i = 0; i < accesoControles.getDtm().getRowCount(); i++) {
                //Variable comodin
                String ejes = "";
                /*Al arduino se le envia los ejes mediante enteros, al posicionar el cursor del for en la columna 0, se obtiene el eje, el switch case asigna
                un numero entero dependiendo el eje que esta leyendo solamente en la columna*/
                switch (accesoControles.getDtm().getValueAt(i, 0).toString()) {
                    case "Pinza":
                        ejes = "1";
                        break;
                    case "Giro de muñeca":
                        ejes = "2";
                        break;
                    case "Muñeca vertical":
                        ejes = "3";
                        break;
                    case "Codo":
                        ejes = "4";
                        break;
                    case "Hombro":
                        ejes = "5";
                        break;
                }
                //Se concatenan todas las columnas de una fila en turno  y se separan los parametros mediante una coma (,), quedando una cadena como por ejemplo  "10,45,90,150, 100, 30"
                //esta variable almacenado todo lo que hay en la tabla, haciendo todas las filas, una sola fila
                movimientos = movimientos + ejes + "," + accesoControles.getDtm().getValueAt(i, 1) + "," + accesoControles.getDtm().getValueAt(i, 2) + "," + accesoControles.getDtm().getValueAt(i, 3) + ",";
            }
            //Se envia a la clase arduino con el identificador 195 y la fila que se obtuvo de la tabla
            arduino.enviarCadena(195, movimientos);

        }
    }

    //Sobrecarga de metodo de la clase ChangeListener
    @Override
    public void stateChanged(ChangeEvent e) {
        //Obtenemos los valores de los JSlider 
        int valorHombro = accesoControles.getJsHombro().getValue();
        int valorCodo = accesoControles.getJsCodo().getValue();
        int valorMuñecaVertical = accesoControles.getJsMuñecaVertical().getValue();
        int valorMuñecaGiro = accesoControles.getJsMuñecaGiro().getValue();
        int valorPinza = accesoControles.getJsPinza().getValue();

        /*Cada eje tiene un identificador en la clase de arduino, al mover algun eje se tienen los siguientes condicionales, dependiendo cual JSlider se este 
        moviendo, se llama al metodo enviarMensaje de clase arduino y se le pasa por parameto el identificador del eje correpondiente que se este moviendo 
        y el valor obtenido, permitiendo ver en tiempo real el movimiento del brazo robot.*/
        if (e.getSource() == accesoControles.getJsHombro()) {
            arduino.enviarMensaje(190, valorHombro);
        }
        if (e.getSource() == accesoControles.getJsCodo()) {
            arduino.enviarMensaje(191, valorCodo);
        }
        if (e.getSource() == accesoControles.getJsMuñecaVertical()) {
            arduino.enviarMensaje(192, valorMuñecaVertical);
        }
        if (e.getSource() == accesoControles.getJsMuñecaGiro()) {
            arduino.enviarMensaje(193, valorMuñecaGiro);
        }
        if (e.getSource() == accesoControles.getJsPinza()) {
            arduino.enviarMensaje(194, valorPinza);

        }

    }

    @Override
    public void mouseClicked(MouseEvent me) {

    }

    @Override
    public void mousePressed(MouseEvent me) {

    }

    //Al finalizar el desplazamiento del JSLider correspondiente al Hombro, se envia a arduino
    //con su identificador, debido al funcionamiento de un motor a pasos se decidio imprementar de esta manera
    @Override
    public void mouseReleased(MouseEvent me) {
        int valorHombro = accesoControles.getJsHombro().getValue();
        arduino.enviarMensaje(190, valorHombro);
    }

    @Override
    public void mouseEntered(MouseEvent me) {

    }

    @Override
    public void mouseExited(MouseEvent me) {

    }

    //Método para guardar los mensajes ingresados a manera de historial, al cerrar la aplicación los mensajes
    //seguiran almacenados en un archivo txt, el parametro que recibe será el nombre con el que se guarde
    private void escribirArchivo(String nombreArchivo) {
        String saltoLinea = "\r\n";
        try {
            //PATH del archivo
            File archivo = new File("src/HistorialInstrucciones/" + nombreArchivo + ".txt");
            //Verificacion de disponibilidad del archivo
            if (archivo != null) {
                FileWriter save = new FileWriter(archivo);

                //Recorrer el arrayList de tipo movimientos donde se almacenó cada objeto de la clase Movimiento por cada linea que se lea de la tabla
                for (int i = 0; i < movimientos.size(); i++) {

                    //Escritura en el archivo
                    save.write(String.valueOf(movimientos.get(i).getEje()));
                    save.write(saltoLinea);
                    save.write(String.valueOf(movimientos.get(i).getGrado()));
                    save.write(saltoLinea);
                    save.write(String.valueOf(movimientos.get(i).getVelocidad()));
                    save.write(saltoLinea);
                    save.write(String.valueOf(movimientos.get(i).getEspera()));
                    save.write(saltoLinea);

                }
                //CIerre del archivo
                save.close();
                //Mensaje de dialogo del estado del proceso
                JOptionPane.showMessageDialog(null,
                        "El archivo se a guardado Exitosamente",
                        "Información", JOptionPane.INFORMATION_MESSAGE);
            }
            //Excepción por si no se encontró la ruta
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null,
                    "Su archivo no se ha guardado",
                    "Advertencia", JOptionPane.WARNING_MESSAGE);
        }
    }

    //Método para el llenado de la tabla a partir de movimientos encontrados del el archivo txt
    public void actualizarTabla() {
        try {
            accesoControles.getDtm().setRowCount(0);

            //Se recorre el arraylist que contiene todos los movimientos almacenados
            for (int i = 0; i < movimientos.size(); i++) {

                Object[] fila = new Object[4];
                String ejes = "";
                //Dependiendo del valor de eje guardado en entero, lo convertimos a texto para mayor facilidad del usuario al cargar rl archivo
                switch (movimientos.get(i).getEje()) {
                    case 1:
                        ejes = "Pinza";
                        break;
                    case 2:
                        ejes = "Giro de muñeca";
                        break;
                    case 3:

                        ejes = "Muñeca vertical";
                        break;
                    case 4:
                        ejes = "Codo";
                        break;
                    case 5:
                        ejes = "Hombro";
                        break;

                }
                 //Colocar cada dato en la columna correspondiente
                fila[0] = ejes;
                fila[1] = movimientos.get(i).getGrado();
                fila[2] = movimientos.get(i).getVelocidad();
                fila[3] = movimientos.get(i).getEspera();
                accesoControles.getDtm().addRow(fila);
            }

            //Actualizacion para redibujar el modelo de la tabla
            accesoControles.getDtm().fireTableDataChanged();
            accesoControles.getTabla().setModel(accesoControles.getDtm());

        } catch (Exception error) {
            System.out.println("error");
        }
    }

    //Metodo correspondiente para abrir un archivo previamente guardado
    public void leerPrograma() {
        //Se limpia el ArrayLIst donde tenemos almacenados objetos de tipo Movimiento y se crean varaibles comodin
        movimientos.clear();
        String aux = "";
        int puntero = 0;
        try {
            //Indicamos una ruta al JFileCHooser para que al momento de seleccionar un archivo se abra en ese PATH indicado
            JFileChooser jfc = new JFileChooser();
            jfc.setCurrentDirectory(new File("src/HistorialInstrucciones/"));
            jfc.showOpenDialog(null);

            //Se abre la ventana correpondiente para seleccionar un archivo en la ruta especificada anteriormente
            File archivo = jfc.getSelectedFile();

            //Si el archivo existe
            if (archivo != null) {
                //Se toma el archivo seleccionado 
                FileReader archivos = new FileReader(archivo);
                BufferedReader lee = new BufferedReader(archivos);
                //Se comienza a leer el archivo linea por linea hasta finalizar
                while ((aux = lee.readLine()) != null) {
                    //dependiendo de la posicion del puntero, sera el valor que se le envie al objeto de tipo Movimiento, al terminar se incrementa el puntero,
                    //de esta forma, al tener los cuatro valores correspondiente, se guarda un objeto con esos valores y se resetea el puntero a 0, esto para crear otro objeto.
                    if (puntero == 0) {
                        mov = new Movimiento();
                        mov.setEje(Integer.parseInt(aux));
                        puntero++;
                    } else if (puntero == 1) {
                        mov.setGrado(Integer.parseInt(aux));
                        puntero++;
                    } else if (puntero == 2) {
                        mov.setVelocidad(Integer.parseInt(aux));
                        puntero++;
                    } else if (puntero == 3) {
                        mov.setEspera(Integer.parseInt(aux));
                        puntero = 0;
                        movimientos.add(mov);
                    }

                }
                //Se cierra el archivo
                lee.close();
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex + ""
                    + "\nNo se ha encontrado el archivo",
                    "ADVERTENCIA!!!", JOptionPane.WARNING_MESSAGE);
        }

    }

}
