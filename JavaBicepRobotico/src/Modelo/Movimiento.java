
package Modelo;

//Clase Movimiento para ir guardando cada fila del JTable como un objeto de tipo movimiento, cada fila representa un paso para el brazo robotico.
public class Movimiento {
    //Variables de clase
    int eje;
    int grado;
    int velocidad;
    int espera;

    public Movimiento() {
    }

    
    //Constructor para crear el objeto con valores especificados
    public Movimiento(int eje, int grado, int velocidad, int espera) {
        this.eje = eje;
        this.grado = grado;
        this.velocidad = velocidad;
        this.espera = espera;
    }

    //Getters y setters
    public int getEje() {
        return eje;
    }

    public void setEje(int eje) {
        this.eje = eje;
    }

    public int getGrado() {
        return grado;
    } 

    public void setGrado(int grado) {
        this.grado = grado;
    }

    public int getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(int velocidad) {
        this.velocidad = velocidad;
    }

    public int getEspera() {
        return espera;
    }

    public void setEspera(int espera) {
        this.espera = espera;
    }
    
    
}
