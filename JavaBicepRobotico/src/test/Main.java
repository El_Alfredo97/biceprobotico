
package test;

import javax.swing.JFrame;
import vista.Home;


public class Main {
    public static void main(String[] args) {
        //Se crea una instancia de la clase Home de tipo JFrame
        Home home = new Home();
        
        //Se asignan propiedades de la ventana principal
        home.setSize(1200, 600);
        home.setLocationRelativeTo(home);
        home.setVisible(true);
        home.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
}
