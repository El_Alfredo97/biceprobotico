package com.example.alfredo.bluetoothmonitor;


import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;

//Clase contrlador de eventos de los botones
public class ControladorJoystick implements View.OnTouchListener {

    //Manejador que nos permite poder enviar datos recurrentemente mientras e deja presionado un botón
    private Handler mHandler;
    //ConnectedTrhead que nos permite poder escribir datos al Arduino desde el controlador
    private MainActivity.ConnectedThread myConexionBT;
    //Objeto de tipo vista para acceder a los botones desde el handler
    View v;

    public ControladorJoystick(MainActivity.ConnectedThread myConexionBT) {
        this.myConexionBT = myConexionBT;
    }

    //Método que se ejecuta cuando se presiona o se suelta un botón
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        this.v = v;
        switch (event.getAction()) {
            //En el caso de que se presione un botón y se deje presionado se estarán mandando constantemente datos a Arduino
            case MotionEvent.ACTION_DOWN:
                if (mHandler != null) return true;

                mHandler = new Handler();
                mHandler.postDelayed(mAction, 15);
                break;
            //E el caso de soltar el boton, se eliminan los Callbacks para que ya no se sigan enviando datos al Arduino
            case MotionEvent.ACTION_UP:
                if (mHandler == null) return true;
                mHandler.removeCallbacks(mAction);
                mHandler = null;
                break;
        }
        return false;
    }
    //Runnable que nos permite enviar datos cada cierto tiempo
    Runnable mAction = new Runnable() {
        @Override public void run() {
            //Según el botón presionado se enviará cierto valor a Arduino para que tome una desición dependiendo del botón
            switch(v.getId()){
                case R.id.btnLeft1:
                    myConexionBT.write(201, 0);
                    System.out.println(201);
                    break;
                case R.id.btnLeft2:
                    myConexionBT.write(202, 0);
                    System.out.println(202);
                    break;
                case R.id.btnUp1:
                    myConexionBT.write(203, 0);
                    System.out.println(203);
                    break;
                case R.id.btnUp2:
                    myConexionBT.write(204, 0);
                    System.out.println(204);
                    break;
                case R.id.btnRight1:
                    myConexionBT.write(205, 0);
                    System.out.println(205);
                    break;
                case R.id.btnRight2:
                    myConexionBT.write(206, 0);
                    System.out.println(206);
                    break;
                case R.id.btnDown1:
                    myConexionBT.write(207, 0);
                    System.out.println(207);
                    break;
                case R.id.btnDown2:
                    myConexionBT.write(208, 0);
                    System.out.println(208);
                    break;
                case R.id.btnPinza1:
                    myConexionBT.write(209, 0);
                    System.out.println(209);
                    break;
                case R.id.btnPinza2:
                    myConexionBT.write(210, 0);
                    System.out.println(210);
                    break;
            }
            //Cada 15 milisegundos se envían los datos
            mHandler.postDelayed(this, 15);

        }
    };
}





