package com.example.alfredo.bluetoothmonitor;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.style.CharacterStyle;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Calendar;
import java.util.UUID;

//Clase principal donde se encuentran los componentes de la interfaz
public class MainActivity extends AppCompatActivity {




    //Botones para controlar el Brazo Robótico
    private Button btnLeft1, btnLeft2, btnUp1, btnUp2, btnRight1, btnRight2, btnDown1, btnDown2, btnPinza1, btnPinza2;

    private ControladorJoystick controlador;                        //Clase controladora que contiene los eventos de los botones al estar presionados o ser soltados
    private Handler bluetoothIn;                                    //Es el handler que permite estra al pendiente de la entrada de los datso desde Bluetooth
    final int handlerState = 0;                                     //Esatdo del handler
    private BluetoothAdapter btAdapter = null;                      //Adaptador de bluetooth que nos permite manipular el funcionamiento de bluetooth
    private BluetoothSocket btSocket = null;                        //Socket para poder establecer conexión con dispositivos bluetooth
    private StringBuilder dataStringIN = new StringBuilder();       //Guarda los mensajes que se reciben por bluetooth
    private ConnectedThread myConexionBT;                           //Maneja el hilo de ejecución que está a la espera de recibir o enviar datos por Bluetooth




    //Identificador único de servicio
    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    //String para la  MAC ADDRESS del adaptador Bluetooth
    private static String address = null;


    //Método sobreescrito donde se crean todos lols componentes cuando se inicia la actividad
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hacer el parseo de los componentes de la interfaz con las variables de Java
        btnLeft1 = findViewById(R.id.btnLeft1);
        btnLeft2 = findViewById(R.id.btnLeft2);
        btnUp1 = findViewById(R.id.btnUp1);
        btnUp2 = findViewById(R.id.btnUp2);
        btnRight1 = findViewById(R.id.btnRight1);
        btnRight2 = findViewById(R.id.btnRight2);
        btnDown1 = findViewById(R.id.btnDown1);
        btnDown2 = findViewById(R.id.btnDown2);
        btnPinza1 = findViewById(R.id.btnPinza1);
        btnPinza2 = findViewById(R.id.btnPinza2);


        //A la espera de recibir mensajes por bluetooth desde Arduino.
        bluetoothIn = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if(msg.what == handlerState){
                    String readMessage = (String) msg.obj;

                    dataStringIN.append(readMessage);
                    //Para encontrar el fin del mensaje se tienen que enviar con el signo gato al final
                    int endOfLineIndex = dataStringIN.indexOf("#");

                    //Mientras no se llegue al final de línea se siguen agregando os bytes a la cadena ya que se envían por partes por el serial.
                    // De esta forma se obtiene el mensaje completo sin cortes
                    if(endOfLineIndex > 0){
                        String dataInPrint = dataStringIN.substring(0, endOfLineIndex);
                        dataStringIN.delete(0, dataStringIN.length());

                        //La cadena recibida contiene 3 valores separados por coma por lo que se dividen y se guardan por separado en un arreglo para luego mosrarlos en los TextView
                        String[] valoresClima = dataInPrint.split(",");

                    }


                }

            }
        };


        //Se procede a revisar si la conexión Bluetooth es posible o está activada
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        verificarEstadoBT();




    }


    //Método para crear un socket de Bluetooth que se usa para la conexión
    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException{
        return device.createInsecureRfcommSocketToServiceRecord(BTMODULEUUID);
    }

   @Override
    protected void onResume() {
        super.onResume();


        //Se detectan los dispositivos Bluetooth vinculados al móvil con una actividad
        Intent intent = getIntent();
        address = intent.getStringExtra(Dispositivos.EXTRA_DEVICES_ADDRESS);

        //Se guarda el dispositivo a conectar, en este caso el HC-05
        BluetoothDevice device = btAdapter.getRemoteDevice(address);

        //Se intenta crear socket de conexion
        try{
            btSocket = createBluetoothSocket(device);

        }catch(IOException e){
            Toast.makeText(getBaseContext(), "La creación del Socket ha fallado", Toast.LENGTH_LONG);

        }

        //Se establece la conexión Bluetooth
        try{
            btSocket.connect();

        }catch(IOException e){
            e.printStackTrace();
            try {
                btSocket.close();
            }catch(IOException e2){

            }
        }

        //Se crea un ConnectedThread que servirá para gestionar el envío y recepción de mensajes al Arduino
        myConexionBT = new ConnectedThread(btSocket);
        myConexionBT.start();

        //Se instancia el controlador de Joystick para poder gestiuonar los eventos de los botones
        controlador = new ControladorJoystick(myConexionBT);

        //Se establece el controlador de eventos para cada botón
        btnLeft1.setOnTouchListener(controlador);
        btnLeft2.setOnTouchListener(controlador);
        btnUp1.setOnTouchListener(controlador);
        btnUp2.setOnTouchListener(controlador);
        btnRight1.setOnTouchListener(controlador);
        btnRight2.setOnTouchListener(controlador);
        btnDown1.setOnTouchListener(controlador);
        btnDown2.setOnTouchListener(controlador);
        btnPinza1.setOnTouchListener(controlador);
        btnPinza2.setOnTouchListener(controlador);
    }


    @Override
    protected void onPause() {
        super.onPause();

        try {
            //Se cierra el socket antes de cerrar la aplicación
            btSocket.close();
        }catch(IOException e){}
    }

    //Método parda verificar el estado de la conexión bluetooth
    private void verificarEstadoBT(){
        if(btAdapter == null){
            //Si no se logra acceder al adpatador bluetooth se muestra una alerta
            Toast.makeText(getBaseContext(), "Hay un problema con la conexión a Bluetooth", Toast.LENGTH_LONG).show();
        }else{
            if(btAdapter.isEnabled()){

            }else{
                //Si se logra detectar el adaptador bluetooth se solicita su activación cuando no está activado el bluetooth
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivity(enableBtIntent);
            }
        }
    }

    //Clase que nos permite gestionar el envío de y recepción de datos al y desde Arduino
    public class ConnectedThread extends Thread{
        //Streams que nos permiten dar los datos a mandar o rescatar los datos enviados desde Arduino
        private final InputStream inStream; //Entrada de datos
        private final OutputStream outStream;   //Saida de datos

        public ConnectedThread (BluetoothSocket socket){
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            }catch(IOException e){

            }

            inStream= tmpIn;
            outStream = tmpOut;


        }

        //Método run del hilo que se encarga de estar a la escucha de los datos que se reciben desde Arduino
        public void run(){
            byte[] buffer = new byte[256];  //Arreglo de bytes para almacenar los datos de Arduino recibidos
            int bytes;  //COntador de bytes
            //Se mantiene en modo escucha para el ingreso de datos
            while(true){
                try {
                    bytes = inStream.read(buffer);
                    String readMessage = new String(buffer, 0, bytes);
                    bluetoothIn.obtainMessage(handlerState, bytes, -1, readMessage).sendToTarget();

                //En caso de error se sale del ciclo
                }catch(IOException e){
                    break;
                }


            }



        }
        //Método para enviar datos al Arduino
        public void write(int opt, int input){
            try {
                outStream.write(opt);
                outStream.write(input);
            }catch(IOException e){
                Toast.makeText(getBaseContext(), "La conexión ha fallado", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }


}
